package plang_test

import (
	"strings"
	"testing"

	"gitlab.com/asvedr/plang"
)

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func between(t *testing.T, val float64, expect float64, d float64) {
	if !(val >= expect-d && val <= expect+d) {
		t.Fatalf("%f != %f +- %f", val, expect, d)
	}
}

const fact = `
# recursive function
fact(x) is if x < 2 ? 1 : x * fact(x - 1).
out res is fact(4). # fact(4) => 1*2*3*4 => 24
`

func TestFullFact(t *testing.T) {
	prog, err := plang.Compile(strings.TrimSpace(fact))
	err_nil(t, err)
	res, err := prog.Execute(1, 5, 10)
	err_nil(t, err)
	if res["res"] != "{avg=24.00, std=0.00}" {
		t.Fatal(res["res"])
	}
}

func TestReadme(t *testing.T) {
	code := `
    doors is range(3). # define 3 doors
    goat is oneof(doors). # put a goat after a random door
    choice_1 is oneof(doors). # player chooses one random door
    host_choice is oneof(doors - u[goat, choice_1]). # host open a door that doesn't have the goat
    choice_2 is oneof(doors - u[choice_1, host_choice]). # player changes it's choice
    out succ1 is choice_1 = goat. # check win probability of first chice
    out succ2 is choice_2 = goat. # check win probability of second choice
	`
	prog, err := plang.Compile(code)
	if err != nil {
		t.Fatal(err.Error())
	}
	_, err = prog.Execute(4, 5000, 10)
	if err != nil {
		t.Fatal(err.Error())
	}
}

func TestIdContainsKW(t *testing.T) {
	code := `is_5 is oneof(range(10)). out res is is_5 = 5.`
	prog, err := plang.Compile(code)
	if err != nil {
		t.Fatal(err.Error())
	}
	_, err = prog.Execute(4, 1000, 10)
	if err != nil {
		t.Fatal(err.Error())
	}
}

func TestFunList(t *testing.T) {
	list := plang.HelpFunList()
	expected := []string{
		"!, %, *, +, -, /, <, <=, <>, =, >, >=, ",
		"and, int, isin, not_uniq, of, oneof, or, ",
		"randbool, randint, randnorm, randprob, ",
		"randreal, range, real, sym, uniq",
	}
	if list != strings.Join(expected, "") {
		t.Fatal(list)
	}
}

func TestFunHelp(t *testing.T) {
	help := plang.HelpFun("oneof")
	if help != "(set) -> any: get random item of set" {
		t.Fatal(help)
	}
}
