# Standart library
## Numeric operations
- standart math operations: +, -, *, /, %
- numeroc comparisons: >, <, >=, <=, =, !=
  WARNING: operators =, != can not be applied to 'real' type
- boolean operations: and, or, =, !=, !
- symbol operations: =, !
- set operations:
  - `range(5), range(0, 5)` generate set of elems between numbers
  - `a + b` join to sets
  - `a - b` return elems of `a` that are not presented in `b`
  - `oneof(a)` take random elem of `a`
  - `x of a` take `x` random elems of `a`
  - `x isin a` true if `x` is presented in `a`
  - `uniq(a)` turn not uniq set into uniq
  - `not_uniq(a)` turn not uniq set into uniq
- random functions:
  - `randbool(prob_of_true)` generate boolean
  - `randint(a,b)` generate random int between `a` and `b`
  - `randreal(a,b)` generate random real betwen `a` and `b`
  - `randprob({a: 0.8, b: 0.2})` return `a` or `b`. Probabilty of `a` is 80% and probability of `b` is 20%
  - `randnorm(mean, dev)` generate random real from normal disturbtion
