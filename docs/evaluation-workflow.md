# Evaluation workflow
Evaludation workflow contains 2 steps. Compilation and Execition

## Compilation
`prog, err := plang.Compile(source_code)`
1. parse code to [grammar model](/grammar/grammar.peg)
2. check names in grammar model, raise error on duplicates, unknown variables/functions
3. compile grammar-functions into [vm-functions](/entities/vm/fun.go) via [bytecode](/entities/vm/bytecode.go)
4. pack functions into [program-container](/impls/builder/program.go)

## Execution
`prog.Execute(goroutine_num, sample_num)`
1. separate samples_num to goroutines
2. run routines with independet environment
3. join ther output
