# Syntax schema
- global value definition
  ```python
  # varname is expression.
  x is 1.
  ```
- global function definition
  ```python
  # funname(arg1,arg2,...) is funbody.
  fact(x) is if x < 2 ? 1 : x * fact(x - 1).
  ```
- conditions
  ```python
  # if condexpr ? thenexpr : elseexpr
  a is if x < 0 ? 1 : 2.
  ```
- local values
  ```python
  # let varname is expr in expr
  f(x) is let a is x + 1 in a*a.
  ```
- primitives
  ```python
  # boolean true
  true
  # boolean false
  false
  # int value: [0-9]+
  1
  # real value: [0-9]+\.[0-9]+
  1.0
  # symbol: '[A-Za-z]+
  'sym
  ```
- set
  ```python
  # set of not unique values [expr, expr, ...]
  [1,1,2,3] != [1,2,3]
  # set of unique values
  u[1,1,2,3] = u[1,2,3]
  ```
- dict
  ```
  # {keyexpr: valexpr, ...}
  {'a: 1, 'b: 2}
  ```
- output
  ```python
  # put 'out' before name in boolean value definition to print a probability of true
  # add type after 'out' keyword for specific format.
  out a is 1 + 2 = 3 # print 'a'. Chooce format automaticly
  out true b is oneof([1,2,3]) = 2 # print probability 'b' = true
  out avg c is oneof([1,2,3]) # print average value of 'c' and standard deviation
  out avgb d is oneof([1,2,3]) # print average value of 'd' and standard deviation (use bigfloat for result calculation)
  out variants e is oneof([1,2,3]) # print map possible values of 'e' with it's probability
  ```
