package plang

import (
	"sort"
	"strings"

	"gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/grammar"
	"gitlab.com/asvedr/plang/impls/builder"
	"gitlab.com/asvedr/plang/impls/compiler"
	"gitlab.com/asvedr/plang/impls/global_env"
	"gitlab.com/asvedr/plang/impls/output"
	"gitlab.com/asvedr/plang/impls/validator"
	"gitlab.com/asvedr/plang/stdlib"
)

type Program interface {
	Execute(
		goroutines int,
		rounds int,
		max_stack int,
	) (map[string]string, error)
}

func HelpFunList() string {
	res := []string{}
	for _, fun := range stdlib.GlobalFuns() {
		name := fun.Name()
		if !strings.HasPrefix(name, ":") {
			res = append(res, name)
		}
	}
	ss := sort.StringSlice(res)
	ss.Sort()
	return strings.Join([]string(ss), ", ")
}

func HelpFun(name string) string {
	for _, fun := range stdlib.GlobalFuns() {
		if fun.Name() == name {
			return fun.Doc()
		}
	}
	return ""
}

func Compile(src string) (Program, error) {
	parsed, err := grammar.Parse(src)
	if err != nil {
		return nil, err
	}
	std := stdlib.GlobalFuns()
	err = validator.New(std).Validate(parsed)
	if err != nil {
		return nil, plangerr.ValidationError{Err: err}
	}
	env := global_env.New()
	for _, fun := range std {
		env.RegFun(fun)
	}
	out := output.New()
	bldr := builder.New(compiler.New(env), env, out)
	return bldr.Build(parsed)
}
