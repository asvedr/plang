package proto

import (
	"gitlab.com/asvedr/plang/entities/output"
	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/grammar"
)

type IGlobalEnv interface {
	IncStack() error
	DecStack()
	GetFun(name string) IFun
	GetVal(name string) value.Value
	RegFun(fun IFun)
	SetVal(name string, val value.Value)
	DropVal(name string)
	Copy(max_stack int) IGlobalEnv
}

type IFun interface {
	Name() string
	Call(IGlobalEnv, []value.Value) (value.Value, error)
	String() string
	Doc() string
}

type IValidator interface {
	Validate(grammar.Program) error
}

type ICompiler interface {
	CompileFun(grammar.Fun) IFun
	CompileVar(grammar.Var) IFun
}

type IBuilder interface {
	Build(grammar.Program) (IProgram, error)
}

type IOutput interface {
	Init(samples int) IOutput
	Add(string, value.Value, output.Fmt) error
	Get() map[string]string
}

type IProgram interface {
	Execute(
		goroutines int,
		rounds int,
		max_stack int,
	) (map[string]string, error)
}
