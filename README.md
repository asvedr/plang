# PLANG (Probability language)

Language designed to simulate processes and calculate their probabilty

## Example
Source task: [Monty Hall problem](https://en.wikipedia.org/wiki/Monty_Hall_problem)
Simulation code:
```python
doors is range(3). # define 3 doors
goat is oneof(doors). # put a goat after a random door
choice_1 is oneof(doors). # player chooses one random door
host_choice is oneof(doors - u[goat, choice_1]). # host open a door that doesn't have the goat
choice_2 is oneof(doors - u[choice_1, host_choice]). # player changes it's choice
out succ1 is choice_1 = goat. # check win probability of the first choice
out succ2 is choice_2 = goat. # check win probability of the second choice
```
Compile and run code:
```go
prog, err := plang.Compile(code)
// Execute program 600 times in 4 goroutines
res, err := prog.Execute(5, 600)
err_nil(t, err)
fmt.Printf("ch1:%.3f ch2:%.3f\n", res["succ1"], res["succ2"])
```
Output:
```go
ch1:0.331 ch2:0.669
```

## Docs
- [evaluation workflow](/docs/evaluation-workflow.md)
- [syntax schema](/docs/syntax.md)
- [stdlib](/docs/stdlib.md)
