package global_env

import (
	errs "gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/proto"
)

type env struct {
	max_stack   int
	stack_depth int
	funs        map[string]proto.IFun
	vars        map[string]value.Value
}

func New() proto.IGlobalEnv {
	return &env{
		funs: map[string]proto.IFun{},
		vars: map[string]value.Value{},
	}
}

func (self *env) GetFun(name string) proto.IFun {
	return self.funs[name]
}

func (self *env) GetVal(name string) value.Value {
	return self.vars[name]
}

func (self *env) DropVal(name string) {
	self.vars[name] = nil
}

func (self *env) RegFun(fun proto.IFun) {
	self.funs[fun.Name()] = fun
}

func (self *env) SetVal(name string, val value.Value) {
	self.vars[name] = val
}

func (self *env) Copy(max_stack int) proto.IGlobalEnv {
	vars := map[string]value.Value{}
	for k, v := range self.vars {
		vars[k] = v
	}
	return &env{
		max_stack: max_stack,
		funs:      self.funs,
		vars:      vars,
	}
}

func (self *env) IncStack() error {
	self.stack_depth += 1
	if self.stack_depth >= self.max_stack {
		return errs.StackOverflow{}
	}
	return nil
}

func (self *env) DecStack() {
	self.stack_depth -= 1
}
