package output

import "fmt"

func percent(p float64) string {
	return fmt.Sprintf("%.2f%%", p*100.0)
}
