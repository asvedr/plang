package output

import (
	"fmt"
	"math"

	"gitlab.com/asvedr/plang/entities/output"
	errs "gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/entities/value"
)

type collector_avg struct {
	name  string
	total int
	nums  []float64
	sum   float64
}

func (*collector_avg) init(name string, samples int) icollector {
	return &collector_avg{
		name:  name,
		total: samples,
		nums:  []float64{},
		sum:   0.0,
	}
}

func (*collector_avg) fmt() output.Fmt {
	return output.Avg
}

func (self *collector_avg) add(val value.Value) error {
	if !val.Type().IsNum() {
		return errs.OutVarInvalidType{
			Var: self.name,
			Tp:  val.Type(),
		}
	}
	real := val.Real()
	self.sum += real
	self.nums = append(self.nums, real)
	return nil
}

// STD=√[(∑(x-x_mid)^2) / n]

func (self *collector_avg) get() string {
	f_total := float64(self.total)
	avg := self.sum / f_total
	var sum float64
	for _, num := range self.nums {
		diff := (num - avg)
		sum += diff * diff
	}
	std := math.Sqrt(sum / f_total)
	return fmt.Sprintf("{avg=%.2f, std=%.2f}", avg, std)
}
