package output

import (
	"fmt"

	"gitlab.com/asvedr/plang/entities/output"
	errs "gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/entities/value"
)

type collector_bool struct {
	name  string
	total int
	succ  int
}

func (*collector_bool) init(name string, total int) icollector {
	return &collector_bool{name: name, total: total}
}

func (*collector_bool) fmt() output.Fmt {
	return output.ProbTrue
}

func (self *collector_bool) add(val value.Value) error {
	if val.Type() != value.VTBool {
		return errs.OutVarInvalidType{Var: self.name, Tp: val.Type()}
	}
	if val.Bool() {
		self.succ += 1
	}
	return nil
}

func (self *collector_bool) get() string {
	p := float64(self.succ) / float64(self.total)
	return fmt.Sprintf("true in %s", percent(p))
}
