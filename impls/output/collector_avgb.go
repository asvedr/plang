package output

import (
	"fmt"
	"math/big"

	"gitlab.com/asvedr/plang/entities/output"
	errs "gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/entities/value"
)

type collector_avgb struct {
	name  string
	total int
	nums  []float64
	sum   *big.Float
}

func (*collector_avgb) init(name string, samples int) icollector {
	return &collector_avgb{
		name:  name,
		total: samples,
		nums:  []float64{},
		sum:   big.NewFloat(0.0),
	}
}

func (*collector_avgb) fmt() output.Fmt {
	return output.Avgb
}

func (self *collector_avgb) add(val value.Value) error {
	if !val.Type().IsNum() {
		return errs.OutVarInvalidType{
			Var: self.name,
			Tp:  val.Type(),
		}
	}
	real := val.Real()
	self.sum.Add(self.sum, big.NewFloat(real))
	self.nums = append(self.nums, real)
	return nil
}

func (self *collector_avgb) get() string {
	_1_d_total := big.NewFloat(1.0 / float64(self.total))
	avg := big.NewFloat(0.0)
	avg.Mul(self.sum, _1_d_total)
	sum := big.NewFloat(0.0)
	for _, num := range self.nums {
		diff := big.NewFloat(num)
		diff.Sub(diff, avg)
		diff.Mul(diff, diff)
		sum.Add(sum, diff)
	}
	sum.Mul(sum, _1_d_total)
	std := sum.Sqrt(sum)
	f_avg, _ := avg.Float64()
	f_std, _ := std.Float64()
	return fmt.Sprintf("{avg=%.2f, std=%.2f}", f_avg, f_std)
}
