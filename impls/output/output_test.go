package output_test

import (
	"testing"

	"gitlab.com/asvedr/plang/entities/output"
	"gitlab.com/asvedr/plang/entities/value"
	impl "gitlab.com/asvedr/plang/impls/output"
)

var maker = impl.New()

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func TestGetEmpty(t *testing.T) {
	out := maker.Init(10)
	if len(out.Get()) != 0 {
		t.Fatal(out.Get())
	}
}

func TestAddGet(t *testing.T) {
	out := maker.Init(4)
	err_nil(t, out.Add("x", value.Bool(true), output.Auto))
	err_nil(t, out.Add("x", value.Bool(true), output.Auto))
	err_nil(t, out.Add("x", value.Bool(false), output.Auto))
	err_nil(t, out.Add("x", value.Bool(false), output.Auto))
	vars := out.Get()
	if vars["x"] != "true in 50.00%" {
		t.Fatal(vars["x"])
	}
}

func TestCopy(t *testing.T) {
	out := maker.Init(2)
	out.Add("x", value.Bool(true), output.Auto)
	out.Add("x", value.Bool(true), output.Auto)
	clone := out.Init(2)
	clone.Add("x", value.Bool(true), output.Auto)
	clone.Add("x", value.Bool(false), output.Auto)
	if out.Get()["x"] != "true in 100.00%" {
		t.Fatal(out.Get()["x"])
	}
	if clone.Get()["x"] != "true in 50.00%" {
		t.Fatal(out.Get()["x"])
	}
}
