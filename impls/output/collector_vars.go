package output

import (
	"fmt"
	"sort"
	"strings"

	"gitlab.com/asvedr/plang/entities/output"
	"gitlab.com/asvedr/plang/entities/value"
)

type collector_vars struct {
	name     string
	total    int
	variants map[string]int
}

func (*collector_vars) init(name string, samples int) icollector {
	return &collector_vars{
		name:     name,
		total:    samples,
		variants: map[string]int{},
	}
}

func (*collector_vars) fmt() output.Fmt {
	return output.ProbVariants
}

func (self *collector_vars) add(val value.Value) error {
	self.variants[val.String()] += 1
	return nil
}

func (self *collector_vars) get() string {
	items := []string{}
	f_total := float64(self.total)
	for key, val := range self.variants {
		p := float64(val) / f_total
		item := fmt.Sprintf("%s: %s", key, percent(p))
		items = append(items, item)
	}
	ss := sort.StringSlice(items)
	ss.Sort()
	items = []string(ss)
	return "{" + strings.Join(items, ", ") + "}"
}
