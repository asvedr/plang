package output

import (
	"gitlab.com/asvedr/plang/entities/output"
	errs "gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/proto"
)

type container struct {
	samples int
	values  map[string]icollector
}

var collector_by_value_type = map[value.ValueType]icollector{
	value.VTBool:   &collector_bool{},
	value.VTInt:    &collector_avg{},
	value.VTReal:   &collector_avg{},
	value.VTSymbol: &collector_vars{},
}

var collector_by_fmt = map[output.Fmt]icollector{
	output.ProbTrue:     &collector_bool{},
	output.ProbVariants: &collector_vars{},
	output.Avg:          &collector_avg{},
	output.Avgb:         &collector_avgb{},
}

type icollector interface {
	init(name string, samples int) icollector
	fmt() output.Fmt
	add(value.Value) error
	get() string
}

func New() proto.IOutput {
	return &container{}
}

func (self *container) Init(samples int) proto.IOutput {
	return &container{
		samples: samples,
		values:  map[string]icollector{},
	}
}

func (self *container) Add(key string, val value.Value, f output.Fmt) error {
	collector := self.values[key]
	var err error
	if collector == nil {
		collector, err = self.init_collector(key, val, f)
		if err != nil {
			return err
		}
		self.values[key] = collector
	}
	return collector.add(val)
}

func (self *container) Get() map[string]string {
	result := map[string]string{}
	for name, collector := range self.values {
		result[name] = collector.get()
	}
	return result
}

func (self *container) init_collector(
	name string,
	val value.Value,
	f output.Fmt,
) (icollector, error) {
	smpl := self.samples
	var specific icollector
	if f != output.Auto {
		specific = collector_by_fmt[f]
	} else {
		specific = collector_by_value_type[val.Type()]
	}
	if specific == nil {
		return nil, errs.CanNotChooseCollector{Var: name}
	}
	return specific.init(name, smpl), nil
}
