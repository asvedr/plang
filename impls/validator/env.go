package validator

type env struct {
	g_funs map[string]bool
	g_vars map[string]bool
	l_vars map[string]bool
}

func new_env() *env {
	return &env{
		g_funs: map[string]bool{},
		g_vars: map[string]bool{},
		l_vars: map[string]bool{},
	}
}

func (self *env) has_fun(name string) bool {
	return self.g_funs[name]
}

func (self *env) has_var(name string) bool {
	return self.g_vars[name] || self.l_vars[name]
}

func (self *env) check_name_used(name string) bool {
	return self.has_fun(name) || self.has_var(name)
}

func (self *env) add_l_var(names ...string) {
	for _, name := range names {
		self.l_vars[name] = true
	}
}

func (self *env) drop_local() {
	self.l_vars = map[string]bool{}
}
