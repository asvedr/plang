package validator

import (
	"fmt"

	errs "gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/grammar"
	"gitlab.com/asvedr/plang/proto"
)

type validator struct {
	stdfuns []proto.IFun
}

func New(stdfuns []proto.IFun) proto.IValidator {
	return validator{stdfuns: stdfuns}
}

func (self validator) Validate(
	program grammar.Program,
) error {
	env := new_env()
	for _, fun := range self.stdfuns {
		env.g_funs[fun.Name()] = true
	}
	for _, fun := range program.Funs {
		if env.g_funs[fun.Name] {
			return errs.GlobalNameAlreadyUsed{Name: fun.Name}
		} else {
			env.g_funs[fun.Name] = true
		}
	}
	var err error
	for _, name := range program.Order {
		fn := program.GetFun(name)
		if fn != nil {
			err = validate_fun(env, *fn)
		} else {
			v := program.GetVar(name)
			err = validate_var(env, *v)
			env.g_vars[name] = true
		}
		if err != nil {
			return err
		}
	}
	return nil
}

func validate_fun(env *env, fun grammar.Fun) error {
	env.drop_local()
	env.add_l_var(fun.Args...)
	return validate_expr(env, fun.Name, fun.Body)
}

func validate_var(env *env, var_ grammar.Var) error {
	env.drop_local()
	if env.check_name_used(var_.Name) {
		return errs.GlobalNameAlreadyUsed{Name: var_.Name}
	}
	return validate_expr(env, var_.Name, var_.Body)
}

func validate_expr(env *env, fun string, i_expr grammar.Expr) error {
	switch expr := i_expr.(type) {
	case *grammar.ExprVar:
		if !env.has_var(expr.Name) {
			return errs.VarNotFound{Where: fun, Var: expr.Name}
		}
	case *grammar.ExprInt:
	case *grammar.ExprReal:
	case *grammar.ExprSym:
		return nil
	case *grammar.ExprCall:
		if !env.has_fun(expr.Fun) {
			return errs.FunNotFound{Where: fun, Fun: expr.Fun}
		}
		return validate_expr_list(env, fun, expr.Args)
	case *grammar.ExprIf:
		return validate_expr_list(
			env,
			fun,
			[]grammar.Expr{expr.If, expr.Then, expr.Else},
		)
	case *grammar.ExprLet:
		validate_let(env, fun, expr)
	default:
		panic(fmt.Sprintf("unexpected expr: %v", i_expr))
	}
	return nil
}

func validate_expr_list(env *env, fun string, exprs []grammar.Expr) error {
	for _, expr := range exprs {
		err := validate_expr(env, fun, expr)
		if err != nil {
			return err
		}
	}
	return nil
}

func validate_let(env *env, fun string, expr *grammar.ExprLet) error {
	err := validate_expr(env, fun, expr.Val)
	if err != nil {
		return err
	}
	orig_val := env.l_vars[expr.Var]
	env.add_l_var(expr.Var)
	err = validate_expr(env, fun, expr.Expr)
	env.l_vars[expr.Var] = orig_val
	return err
}
