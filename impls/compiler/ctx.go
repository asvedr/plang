package compiler

type var_type int

const (
	var_arg var_type = iota
	var_loc
	var_glob
)

type ctx struct {
	stack_size    int
	current_stack int
	args          map[string]int
	loc_var_to_id map[string]int
	env_size      int
}

func new_ctx(args []string) *ctx {
	map_args := map[string]int{}
	for i, arg := range args {
		map_args[arg] = i
	}
	return &ctx{
		args:          map_args,
		loc_var_to_id: map[string]int{},
	}
}

func (self *ctx) clone() *ctx {
	copy := *self
	copy.args = copy_map(self.args)
	copy.loc_var_to_id = copy_map(self.loc_var_to_id)
	return &copy
}

func copy_map(src map[string]int) map[string]int {
	res := map[string]int{}
	for k, v := range src {
		res[k] = v
	}
	return res
}

func (self *ctx) merge(other *ctx) {
	self.stack_size = max(self.stack_size, other.stack_size)
	self.current_stack = max(self.current_stack, other.current_stack)
	self.env_size = max(self.env_size, other.env_size)
}

func (self *ctx) push() {
	self.current_stack += 1
	if self.current_stack > self.stack_size {
		self.stack_size = self.current_stack
	}
}

func (self *ctx) pop() {
	self.current_stack -= 1
}

func (self *ctx) new_var(name string) int {
	id := self.env_size
	self.loc_var_to_id[name] = id
	self.env_size += 1
	return id
}

func (self *ctx) set_var(name string, id int) {
	self.loc_var_to_id[name] = id
}

func (self *ctx) get_id(name string) (int, var_type) {
	id, found := self.loc_var_to_id[name]
	if found {
		return id, var_loc
	}
	id, found = self.args[name]
	if found {
		return id, var_arg
	}
	return -1, var_glob
}
