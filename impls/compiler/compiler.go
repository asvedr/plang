package compiler

import (
	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/entities/vm"
	"gitlab.com/asvedr/plang/grammar"
	"gitlab.com/asvedr/plang/proto"
)

type compiler struct {
	env proto.IGlobalEnv
}

func New(env proto.IGlobalEnv) proto.ICompiler {
	return compiler{env: env}
}

func (self compiler) CompileFun(fun grammar.Fun) proto.IFun {
	ctx := new_ctx(fun.Args)
	code := self.compile_expr(ctx, fun.Body)
	code = append(code, vm.CmdRet())
	return vm.NewFun(
		fun.Name,
		code,
		ctx.stack_size,
		ctx.env_size,
		len(fun.Args),
	)
}

func (self compiler) CompileVar(expr grammar.Var) proto.IFun {
	ctx := new_ctx([]string{})
	code := self.compile_expr(ctx, expr.Body)
	code = append(code, vm.CmdRet())
	name := expr.Name
	return vm.NewFun(name, code, ctx.stack_size, ctx.env_size, 0)
}

func (self compiler) compile_expr(ctx *ctx, expr grammar.Expr) []vm.Code {
	switch spec := expr.(type) {
	case *grammar.ExprInt:
		ctx.push()
		return []vm.Code{vm.CmdVal(value.Int(spec.Val))}
	case *grammar.ExprBool:
		ctx.push()
		return []vm.Code{vm.CmdVal(value.Bool(spec.Val))}
	case *grammar.ExprReal:
		ctx.push()
		return []vm.Code{vm.CmdVal(value.Real(spec.Val))}
	case *grammar.ExprSym:
		ctx.push()
		return []vm.Code{vm.CmdVal(value.Symbol(spec.Val))}
	case *grammar.ExprVar:
		ctx.push()
		return self.get_var(ctx, spec)
	case *grammar.ExprCall:
		return self.call(ctx, spec)
	case *grammar.ExprIf:
		return self.if_(ctx, spec)
	case *grammar.ExprLet:
		return self.let(ctx, spec)
	default:
		panic("unexpected expr: " + expr.String())
	}
}

func (self compiler) get_var(ctx *ctx, expr *grammar.ExprVar) []vm.Code {
	id, tp := ctx.get_id(expr.Name)
	var instr vm.Code
	switch tp {
	case var_arg:
		instr = vm.CmdGetArg(id)
	case var_glob:
		instr = vm.CmdGetGlobVar(expr.Name)
	case var_loc:
		instr = vm.CmdGetLocVar(id)
	default:
		panic("unknown var type")
	}
	return []vm.Code{instr}
}

func (self compiler) call(ctx *ctx, spec *grammar.ExprCall) []vm.Code {
	var result []vm.Code
	for _, arg := range spec.Args {
		result = append(result, self.compile_expr(ctx, arg)...)
	}
	fun := self.env.GetFun(spec.Fun)
	var instr vm.Code
	for range spec.Args {
		ctx.pop()
	}
	if fun != nil {
		instr = vm.CmdCallFun(fun, len(spec.Args))
	} else {
		instr = vm.CmdCallName(spec.Fun, len(spec.Args))
	}
	ctx.push()
	return append(result, instr)
}

func (self compiler) let(ctx *ctx, spec *grammar.ExprLet) []vm.Code {
	result := self.compile_expr(ctx, spec.Val)
	prev_id, prev_tp := ctx.get_id(spec.Var)
	ctx.pop()
	instr := vm.CmdSetLocVar(ctx.new_var(spec.Var))
	result = append(result, instr)
	result = append(result, self.compile_expr(ctx, spec.Expr)...)
	if prev_tp == var_loc {
		ctx.set_var(spec.Var, prev_id)
	}
	return result
}

func (self compiler) if_(ctx *ctx, spec *grammar.ExprIf) []vm.Code {
	result := self.compile_expr(ctx, spec.If)
	ctx.pop()
	t_ctx := ctx.clone()
	e_ctx := ctx.clone()
	then_ := self.compile_expr(t_ctx, spec.Then)
	else_ := self.compile_expr(e_ctx, spec.Else)
	ctx.merge(t_ctx)
	ctx.merge(e_ctx)
	then_ = append(then_, vm.CmdJump(len(else_)+1))
	result = append(result, vm.CmdJumpOnFalse(len(then_)+1))
	result = append(result, then_...)
	return append(result, else_...)
}
