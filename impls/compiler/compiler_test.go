package compiler_test

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/grammar"
	"gitlab.com/asvedr/plang/impls/compiler"
	"gitlab.com/asvedr/plang/impls/global_env"
	"gitlab.com/asvedr/plang/proto"
	"gitlab.com/asvedr/plang/stdlib"
)

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func equal(t *testing.T, a, b any) {
	if !reflect.DeepEqual(a, b) {
		t.Fatalf("%v != %v", a, b)
	}
}

func new_compiler() proto.ICompiler {
	env := global_env.New()
	for _, fun := range stdlib.GlobalFuns() {
		env.RegFun(fun)
	}
	return compiler.New(env)
}

const a_plus_b_src = `f(a, b) is a + b.`
const a_plus_b_code = `fun 'f' (stack=2, env=0, args=2):
  {get_arg, v:nil, s:"", i:0, f:nil}
  {get_arg, v:nil, s:"", i:1, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'+'}
  {ret, v:nil, s:"", i:0, f:nil}
`

func TestCompileAplusB(t *testing.T) {
	prog, err := grammar.Parse(a_plus_b_src)
	err_nil(t, err)
	fun := new_compiler().CompileFun(prog.Funs[0])
	equal(t, a_plus_b_code, fun.String())

	args := []value.Value{value.Int(1), value.Int(2)}
	res, err := fun.Call(global_env.New().Copy(10), args)
	err_nil(t, err)
	equal(t, int64(3), res.Int())
}

const simple_if_src = `f(a) is if a < 0 ? 'l : 'g.`
const simple_if_code = `fun 'f' (stack=2, env=0, args=1):
  {get_arg, v:nil, s:"", i:0, f:nil}
  {val, v:0, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'<'}
  {jump_on_false, v:nil, s:"", i:3, f:nil}
  {val, v:'l, s:"", i:0, f:nil}
  {jump, v:nil, s:"", i:2, f:nil}
  {val, v:'g, s:"", i:0, f:nil}
  {ret, v:nil, s:"", i:0, f:nil}
`

func TestSimpleIf(t *testing.T) {
	prog, err := grammar.Parse(simple_if_src)
	err_nil(t, err)
	fun := new_compiler().CompileFun(prog.Funs[0])
	equal(t, simple_if_code, fun.String())

	res, err := fun.Call(global_env.New().Copy(10), []value.Value{value.Int(1)})
	err_nil(t, err)
	equal(t, "g", res.Symbol())
	res, err = fun.Call(global_env.New().Copy(10), []value.Value{value.Int(-1)})
	err_nil(t, err)
	equal(t, "l", res.Symbol())
}

const simple_let_src = `f(a) is let x is a + 1 in x * a.`
const simple_let_code = `fun 'f' (stack=2, env=1, args=1):
  {get_arg, v:nil, s:"", i:0, f:nil}
  {val, v:1, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'+'}
  {set_loc, v:nil, s:"", i:0, f:nil}
  {get_loc, v:nil, s:"", i:0, f:nil}
  {get_arg, v:nil, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'*'}
  {ret, v:nil, s:"", i:0, f:nil}
`

func TestSimpleLet(t *testing.T) {
	prog, err := grammar.Parse(simple_let_src)
	err_nil(t, err)
	fun := new_compiler().CompileFun(prog.Funs[0])
	equal(t, simple_let_code, fun.String())

	res, err := fun.Call(global_env.New().Copy(10), []value.Value{value.Int(2)})
	err_nil(t, err)
	equal(t, int64(6), res.Int())
}

const sublet_src = `
	f(a) is let z is if a > 0
			? let x is -a in x * x
			: let y is a in y * y
		in z * 2.
`
const sublet_code = `fun 'f' (stack=2, env=2, args=1):
  {get_arg, v:nil, s:"", i:0, f:nil}
  {val, v:0, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'>'}
  {jump_on_false, v:nil, s:"", i:8, f:nil}
  {get_arg, v:nil, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:1, f:'-'}
  {set_loc, v:nil, s:"", i:0, f:nil}
  {get_loc, v:nil, s:"", i:0, f:nil}
  {get_loc, v:nil, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'*'}
  {jump, v:nil, s:"", i:6, f:nil}
  {get_arg, v:nil, s:"", i:0, f:nil}
  {set_loc, v:nil, s:"", i:0, f:nil}
  {get_loc, v:nil, s:"", i:0, f:nil}
  {get_loc, v:nil, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'*'}
  {set_loc, v:nil, s:"", i:1, f:nil}
  {get_loc, v:nil, s:"", i:1, f:nil}
  {val, v:2, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'*'}
  {ret, v:nil, s:"", i:0, f:nil}
`

func TestSublet(t *testing.T) {
	prog, err := grammar.Parse(strings.TrimSpace(sublet_src))
	err_nil(t, err)
	fun := new_compiler().CompileFun(prog.Funs[0])
	equal(t, sublet_code, fun.String())
}

const fact_src = `fact(x) is if x < 2? 1: x * fact(x - 1).`
const fact_code = `fun 'fact' (stack=3, env=0, args=1):
  {get_arg, v:nil, s:"", i:0, f:nil}
  {val, v:2, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'<'}
  {jump_on_false, v:nil, s:"", i:3, f:nil}
  {val, v:1, s:"", i:0, f:nil}
  {jump, v:nil, s:"", i:7, f:nil}
  {get_arg, v:nil, s:"", i:0, f:nil}
  {get_arg, v:nil, s:"", i:0, f:nil}
  {val, v:1, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'-'}
  {call_name, v:nil, s:"fact", i:1, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'*'}
  {ret, v:nil, s:"", i:0, f:nil}
`

func TestFact(t *testing.T) {
	prog, err := grammar.Parse(fact_src)
	err_nil(t, err)
	fun := new_compiler().CompileFun(prog.Funs[0])
	equal(t, fact_code, fun.String())

	env := global_env.New().Copy(10)
	env.RegFun(fun)

	res, err := fun.Call(env, []value.Value{value.Int(0)})
	err_nil(t, err)
	equal(t, int64(1), res.Int())
	res, err = fun.Call(env, []value.Value{value.Int(3)})
	err_nil(t, err)
	equal(t, int64(6), res.Int())
}

const bool_code = `fun 'f' (stack=2, env=0, args=1):
  {val, v:true, s:"", i:0, f:nil}
  {val, v:false, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'<>'}
  {ret, v:nil, s:"", i:0, f:nil}
`

func TestBool(t *testing.T) {
	prog, err := grammar.Parse("f(x) is true <> false.")
	err_nil(t, err)
	fun := new_compiler().CompileFun(prog.Funs[0])
	equal(t, bool_code, fun.String())
}
