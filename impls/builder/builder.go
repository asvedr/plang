package builder

import (
	"gitlab.com/asvedr/plang/entities/output"
	"gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/grammar"
	"gitlab.com/asvedr/plang/proto"
)

type builder struct {
	compiler proto.ICompiler
	env      proto.IGlobalEnv
	output   proto.IOutput
}

func New(
	compiler proto.ICompiler,
	env proto.IGlobalEnv,
	output proto.IOutput,
) proto.IBuilder {
	return builder{
		compiler: compiler,
		env:      env,
		output:   output,
	}
}

func (self builder) Build(src grammar.Program) (proto.IProgram, error) {
	for _, src_fun := range src.Funs {
		fun := self.compiler.CompileFun(src_fun)
		if self.env.GetFun(src_fun.Name) != nil {
			err := plangerr.GlobalNameAlreadyUsed{Name: src_fun.Name}
			return nil, err
		}
		self.env.RegFun(fun)
	}
	vars := []proto.IFun{}
	used_vars := map[string]bool{}
	vars_to_print := map[string]output.Fmt{}
	for _, src_var := range src.Vars {
		if src_var.ToPrint {
			vars_to_print[src_var.Name] = src_var.Fmt
		}
		fun := self.compiler.CompileVar(src_var)
		has_std_var := self.env.GetVal(src_var.Name) != nil
		if has_std_var || used_vars[src_var.Name] {
			err := plangerr.GlobalNameAlreadyUsed{Name: src_var.Name}
			return nil, err
		}
		used_vars[src_var.Name] = true
		vars = append(vars, fun)
	}
	prog := &program{
		env:              self.env,
		var_initializers: vars,
		output:           self.output,
		vars_to_print:    vars_to_print,
	}
	return prog, nil
}
