package builder

import (
	"errors"
	"sync"

	"gitlab.com/asvedr/plang/entities/output"
	"gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/proto"
)

type program struct {
	env              proto.IGlobalEnv
	var_initializers []proto.IFun
	vars_to_print    map[string]output.Fmt
	output           proto.IOutput
}

type context struct {
	err_chan   chan error
	output     proto.IOutput
	output_mtx sync.Mutex
	rounds     int
	max_stack  int
}

func (self *program) Execute(
	goroutines int,
	rounds int,
	max_stack int,
) (map[string]string, error) {
	per_routine := rounds / goroutines
	ctx := &context{
		err_chan:  make(chan error),
		output:    self.output.Init(per_routine * goroutines),
		rounds:    per_routine,
		max_stack: max_stack,
	}
	for i := 0; i < goroutines; i += 1 {
		go func() {
			ctx.err_chan <- self.worker(ctx)
		}()
	}
	for i := 0; i < goroutines; i += 1 {
		err := <-ctx.err_chan
		if err != nil {
			return nil, err
		}
	}
	return ctx.output.Get(), nil
}

func (self *program) worker(ctx *context) error {
	for i := 0; i < ctx.rounds; i += 1 {
		err := self.round(ctx)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self *program) round(ctx *context) error {
	type outvar struct {
		name string
		val  value.Value
		fmt  output.Fmt
	}

	env := self.env.Copy(ctx.max_stack)
	outvars := []outvar{}
	for _, fun := range self.var_initializers {
		val, err := fun.Call(env, nil)
		if err != nil {
			return self.make_rt_error(env, err)
		}
		name := fun.Name()
		env.SetVal(name, val)
		f, found := self.vars_to_print[name]
		if found {
			ov := outvar{name: name, val: val, fmt: f}
			outvars = append(outvars, ov)
		}
		if err != nil {
			return self.make_rt_error(env, err)
		}
	}
	ctx.output_mtx.Lock()
	defer ctx.output_mtx.Unlock()
	for _, outvar := range outvars {
		err := ctx.output.Add(outvar.name, outvar.val, outvar.fmt)
		if err != nil {
			return self.make_rt_error(env, err)
		}
	}
	return nil
}

func (self *program) make_rt_error(env proto.IGlobalEnv, err error) error {
	vars := map[string]plangerr.ToStr{}
	if errors.Is(err, plangerr.StackOverflow{}) {
		return plangerr.RuntimeError{
			Err:  err,
			Vars: vars,
		}
	}
	for _, fun := range self.var_initializers {
		name := fun.Name()
		vars[name] = env.GetVal(name)
	}
	return plangerr.RuntimeError{
		Err:  err,
		Vars: vars,
	}
}
