package set_test

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/asvedr/plang"
)

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func between(t *testing.T, val string, num int) {
	for _, d := range []int{-1, 0, 1} {
		pref := fmt.Sprintf("true in %d", num+d)
		if strings.HasPrefix(val, pref) {
			return
		}
	}
	t.Fatalf("%s is not %d", val, num)
}

const one_of = `
doors is u['a, 'b, 'c].
winner is oneof(doors).
choice is oneof(doors).
out succ is winner = choice.
`

func TestOneOf(t *testing.T) {
	prog, err := plang.Compile(one_of)
	err_nil(t, err)
	res, err := prog.Execute(4, 4000, 10)
	err_nil(t, err)
	succ := res["succ"]
	between(t, succ, 33)
}

const n_of = `
tickets is range(10).
winners is 3 of tickets.
choice is 7.
out succ is choice isin winners.
`

func TestNOf(t *testing.T) {
	prog, err := plang.Compile(n_of)
	err_nil(t, err)
	res, err := prog.Execute(5, 6000, 10)
	err_nil(t, err)
	succ := res["succ"]
	between(t, succ, 30)
}

const exclude_code = `
doors is u['a, 'b, 'c].
choice is oneof(doors - ['c]).
out a is choice = 'a.
out b is choice = 'b.
out c is choice = 'c.
out always_a is oneof(doors - ['b, 'c]) = 'a.
`

func TestExclude(t *testing.T) {
	prog, err := plang.Compile(exclude_code)
	err_nil(t, err)
	res, err := prog.Execute(3, 6000, 10)
	err_nil(t, err)

	between(t, res["a"], 50)
	between(t, res["b"], 50)
	between(t, res["c"], 0)
	between(t, res["always_a"], 100)
}
