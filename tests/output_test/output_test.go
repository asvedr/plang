package output_test

import (
	"regexp"
	"testing"

	"gitlab.com/asvedr/plang"
)

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func TestOutBool(t *testing.T) {
	prog, _ := plang.Compile("out true X is 1 = 2.")
	res, err := prog.Execute(2, 1000, 10)
	err_nil(t, err)
	if res["x"] != "true in 0.00%" {
		t.Fatal(res["x"])
	}
	prog, _ = plang.Compile("out true X is 1.")
	_, err = prog.Execute(2, 5, 10)
	msg := err.Error()
	exp := "Runtime error: output var 'x' has invalid type: int\n  x = 1\n"
	if msg != exp {
		t.Fatal(err)
	}
}

func TestOutVars(t *testing.T) {
	prog, _ := plang.Compile("out variants X is oneof([1,1,1,2,1,3]).")
	res, err := prog.Execute(4, 6000, 10)
	err_nil(t, err)
	matched, err := regexp.Match(
		"{1: 6....%, 2: 1....%, 3: 1....%}",
		[]byte(res["x"]),
	)
	err_nil(t, err)
	if !matched {
		t.Fatal(res["x"])
	}
}

func TestOutAvg(t *testing.T) {
	prog, _ := plang.Compile("out avg X is oneof([1,1,1,2,1,3]).")
	res, err := prog.Execute(4, 6000, 10)
	err_nil(t, err)
	matched, _ := regexp.Match(
		"{avg=1.(4|5)., std=0.7.}",
		[]byte(res["x"]),
	)
	if !matched {
		t.Fatal(res["x"])
	}
}

func TestOutAvgb(t *testing.T) {
	prog, err := plang.Compile("out avgb x is oneof([1,1,1,2,1,3]).")
	err_nil(t, err)
	res, err := prog.Execute(4, 6000, 10)
	err_nil(t, err)
	matched, _ := regexp.Match(
		"{avg=1.(4|5)., std=0.7.}",
		[]byte(res["x"]),
	)
	if !matched {
		t.Fatal(res["x"])
	}
}
