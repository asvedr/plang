package rand_funs_test

import (
	"math"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/asvedr/plang"
)

func TestRandReal(t *testing.T) {
	prog, err := plang.Compile(`
		out avg A is randreal(0, 100).
		out avg B is randnorm(50, 10).
	`)
	if err != nil {
		t.Fatal(err.Error())
	}
	res, err := prog.Execute(4, 1000, 10)
	if err != nil {
		t.Fatal(err)
	}
	avg, std := parse_res(res["a"])
	if math.Abs(avg-50.0) > 2.0 || math.Abs(std-30.0) > 2.0 {
		t.Fatalf("> %f %f", avg, std)
	}
	avg, std = parse_res(res["b"])
	if math.Abs(avg-50.0) > 2.0 || math.Abs(std-10.0) > 2.0 {
		t.Fatalf("> %f %f", avg, std)
	}
}

func TestRandBool(t *testing.T) {
	prog, err := plang.Compile(`out A is randbool(0.8).`)
	if err != nil {
		t.Fatal(t, err)
	}
	res, err := prog.Execute(4, 5000, 10)
	if err != nil {
		t.Fatal(t, err)
	}
	s_val := res["a"]
	trimmed := strings.TrimSuffix(
		strings.TrimPrefix(s_val, "true in "),
		"%",
	)
	prob, err := strconv.ParseFloat(trimmed, 64)
	if err != nil {
		t.Fatal(err.Error())
	}
	if math.Abs(prob-80.0) > 2.0 {
		t.Fatal(s_val)
	}
}

func parse_res(src string) (float64, float64) {
	f := func(src string) float64 {
		data := strings.Split(src, "=")[1]
		val, err := strconv.ParseFloat(data, 64)
		if err != nil {
			panic(err.Error())
		}
		return val
	}

	src = strings.ReplaceAll(strings.ReplaceAll(src, "{", ""), "}", "")
	tokens := strings.Split(src, ", ")
	return f(tokens[0]), f(tokens[1])
}
