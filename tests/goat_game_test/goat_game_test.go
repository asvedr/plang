package goat_game_test

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/asvedr/plang"
)

const code = `
doors is ['a, 'b, 'c].
goat is oneof(doors).
choice_1 is oneof(doors).
host_choice is oneof(doors - u[goat, choice_1]).
choice_2 is oneof(doors - u[choice_1, host_choice]).
out choice_1_succ is choice_1 = goat.
out choice_2_succ is choice_2 = goat.
`

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func between(t *testing.T, val string, num int) {
	for _, d := range []int{-1, 0, 1} {
		pref := fmt.Sprintf("true in %d", num+d)
		if strings.HasPrefix(val, pref) {
			return
		}
	}
	t.Fatalf("%s is not %d", val, num)
}

func TestGoatGame(t *testing.T) {
	prog, err := plang.Compile(code)
	err_nil(t, err)
	res, err := prog.Execute(5, 6000, 10)
	err_nil(t, err)
	choice1 := res["choice_1_succ"]
	choice2 := res["choice_2_succ"]
	between(t, choice1, 33)
	between(t, choice2, 66)
}
