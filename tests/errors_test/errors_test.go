package errors_test

import (
	"strings"
	"testing"

	"gitlab.com/asvedr/plang"
)

func TestPrintNotBool(t *testing.T) {
	prog, err := plang.Compile(`out x is 3 of [1,2,3,4,5,6].`)
	if err != nil {
		t.Fatal(err.Error())
	}
	_, err = prog.Execute(1, 3, 10)
	if err == nil {
		t.Fatal("Expected error")
	}
	txt := strings.Split(err.Error(), "\n")[0]
	if txt != "Runtime error: can not choose collector for var: 'x'" {
		t.Fatal(err.Error())
	}
}

const var_not_found_code = `
tickets is range(10).
winners is 3 of tickets.
choice is 7.
out succ is winner = choice.
`

func TestVarNotFound(t *testing.T) {
	_, err := plang.Compile(var_not_found_code)
	if err == nil {
		t.Fatal("expected error")
	}
	expected := "validation error: VarNotFound(where: 'succ', var: 'winner')"
	if err.Error() != expected {
		t.Fatal(err.Error())
	}
}

const cond_not_bool_code = `
sick is randprob({'a: 0.5, 'b: 0.5}).
onsick is randprob({'a: 0.8, 'b: 0.2}). 
onhealthy is randprob({'a: 0.4, 'b: 0.6}). 
test is if sick ? onsick : onhealthy.
out res is sick = test.
`

func TestCondNotBool(t *testing.T) {
	prog, err := plang.Compile(cond_not_bool_code)
	if err != nil {
		t.Fatal(err)
	}
	_, err = prog.Execute(1, 100, 10)
	if err == nil {
		t.Fatal("expected error")
	}
	got := strings.Split(err.Error(), "\n")[0]
	expected := "Runtime error: condition must be bool but it's: symbol"
	if got != expected {
		t.Fatal(err)
	}
}
