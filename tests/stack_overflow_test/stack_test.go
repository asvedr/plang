package stack_overflow_test

import (
	"strings"
	"testing"

	"gitlab.com/asvedr/plang"
)

const inf_rec = `
f(x) is [f(x+1), f(x+2), f(x+3)].
out variants res is f(3).
`

func TestOverflow(t *testing.T) {
	prog, err := plang.Compile(inf_rec)
	if err != nil {
		t.Fatal(err)
	}
	_, err = prog.Execute(1, 200, 100)
	msg := strings.TrimSpace(err.Error())
	if msg != "Runtime error: stack overflow(possible infinity recursion)" {
		t.Fatal(err)
	}
}
