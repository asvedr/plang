package prob_choice_test

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/asvedr/plang"
)

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func between(t *testing.T, val string, num int) {
	for _, d := range []int{-1, 0, 1} {
		pref := fmt.Sprintf("true in %d", num+d)
		if strings.HasPrefix(val, pref) {
			return
		}
	}
	t.Fatalf("%s is not %d", val, num)
}

const code_50_50 = `
choice is randprob({'a: 1, 'b: 1}).
out a is choice = 'a.
out b is choice = 'b.
`

func Test_50_50(t *testing.T) {
	prog, err := plang.Compile(code_50_50)
	err_nil(t, err)
	res, err := prog.Execute(3, 6000, 10)
	err_nil(t, err)
	between(t, res["a"], 50)
	between(t, res["b"], 50)
}

const code_80_20 = `
choice is randprob({'a: 0.8, 'b: 0.2}).
out a is choice = 'a.
out b is choice = 'b.
`

func Test_80_20(t *testing.T) {
	prog, err := plang.Compile(code_80_20)
	err_nil(t, err)
	res, err := prog.Execute(3, 5000, 10)
	err_nil(t, err)
	between(t, res["a"], 80)
	between(t, res["b"], 20)
}
