package grammar

import (
	"fmt"
)

var priority_map = map[string]int{
	"and":  1,
	"or":   1,
	"isin": 2,
	"=":    2,
	"<>":   2,
	">=":   2,
	"<=":   2,
	">":    2,
	"<":    2,
	"+":    3,
	"-":    3,
	"*":    4,
	"/":    4,
	"%":    4,
	"of":   5,
}

func build_op_seq(seq []Expr) Expr {
	if len(seq) == 1 {
		return seq[0]
	}
	i := find_mininal_op_index(seq)
	left := build_op_seq(seq[:i])
	right := build_op_seq(seq[i+1:])
	op := seq[i].(*Operator)
	return &ExprCall{Fun: op.Name, Args: []Expr{left, right}}
}

func find_mininal_op_index(seq []Expr) int {
	min_index := -1
	var min_prior int
	for i, item := range seq {
		op, casted := item.(*Operator)
		if !casted {
			continue
		}
		prior, found := priority_map[op.Name]
		if !found {
			panic("unregisred op: '" + op.Name + "'")
		}
		if min_index < 0 || prior <= min_prior {
			min_index = i
			min_prior = prior
		}
	}
	if min_index < 0 {
		panic(fmt.Sprintf("min op not found: %v", seq))
	}
	return min_index
}
