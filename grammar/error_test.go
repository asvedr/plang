package grammar_test

import (
	"strings"
	"testing"

	g "gitlab.com/asvedr/plang/grammar"
)

const expected = `
Parse error
  row: "out a is b is."
after seq:
  "out a is b "
`

func TestParseError(t *testing.T) {
	_, err := g.Parse("out a is b is.")
	ts := strings.TrimSpace
	if ts(err.Error()) != ts(expected) {
		t.Fatal(err.Error())
	}
}
