package grammar

import "strings"

func RemoveComments(src string) string {
	var begin int
	in_comment := false
	positions := map[int]int{}
	for i, sym := range src {
		if !in_comment {
			if sym == '#' {
				begin = i
				in_comment = true
			}
		} else {
			if sym == '\n' {
				positions[begin] = i
				in_comment = false
			}
		}
	}
	if in_comment {
		positions[begin] = len(src)
	}
	for from, to := range positions {
		before := src[:from]
		after := src[to:]
		mid := strings.Repeat(" ", to-from)
		src = before + mid + after
	}
	return src
}
