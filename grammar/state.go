package grammar

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/asvedr/gstack"
	"gitlab.com/asvedr/plang/entities/output"
)

type PlangState struct {
	reg              registers
	fun_name         string
	var_printable    bool
	var_printable_tp string
	var_name         string
	fun_args         []string
	functions        []Fun
	vars             []Var
	stack            gstack.Stack[registers]
	err              error
	order            []string
}

type registers struct {
	cond_if   Expr
	cond_then Expr
	expr      Expr
	let_var   string
	let_val   Expr
	op_seq    []Expr
	id        string
	fun_name  string
	fun_args  []Expr
}

func (self *PlangState) Init() {
	self.stack = gstack.NewHeap[registers]()
}

func (self *PlangState) parse_int(src string) {
	val, err := strconv.ParseInt(src, 10, 64)
	self.reg.expr = &ExprInt{Val: val}
	if err != nil {
		self.err = err
	}
}

func (self *PlangState) parse_true() {
	self.reg.expr = &ExprBool{Val: true}
}

func (self *PlangState) parse_false() {
	self.reg.expr = &ExprBool{Val: false}
}

func (self *PlangState) parse_float(src string) {
	val, err := strconv.ParseFloat(src, 64)
	self.reg.expr = &ExprReal{Val: val}
	if err != nil {
		self.err = err
	}
}

func (self *PlangState) parse_sym(src string) {
	self.reg.expr = &ExprSym{Val: src}
}

func (self *PlangState) construct_var() {
	self.reg.expr = &ExprVar{Name: self.reg.id}
}

func (self *PlangState) construct_call_arg() {
	self.reg.fun_args = append(
		self.reg.fun_args,
		self.reg.expr,
	)
}

func (self *PlangState) set_id(src string) {
	self.reg.id = strings.ToLower(src)
}

func (self *PlangState) push() {
	self.stack.Push(self.reg)
	self.reg = registers{}
}

func (self *PlangState) pop() {
	reg, found := self.stack.PopValue()
	if !found {
		panic("grammar: pop empty stack")
	}
	self.reg = reg
}

func (self *PlangState) start_call() {
	name := self.reg.id
	self.push()
	self.reg.fun_name = name
	self.reg.fun_args = []Expr{}
}

func (self *PlangState) end_call() {
	expr := &ExprCall{
		Fun:  self.reg.fun_name,
		Args: self.reg.fun_args,
	}
	self.pop()
	self.reg.expr = expr
}

func (self *PlangState) start_un_op_call(src string) {
	self.set_id(src)
	self.start_call()
}

func (self *PlangState) end_un_op_call() {
	self.construct_call_arg()
	self.end_call()
}

func (self *PlangState) push_bin_op(opr string) {
	self.reg.op_seq = append(
		self.reg.op_seq,
		self.reg.expr,
		&Operator{Name: opr},
	)
}

func (self *PlangState) compose_op_seq() {
	self.reg.op_seq = append(
		self.reg.op_seq,
		self.reg.expr,
	)
	self.reg.expr = build_op_seq(self.reg.op_seq)
	self.reg.op_seq = nil
}

func (self *PlangState) start_then() {
	self.reg.cond_if = self.reg.expr
}

func (self *PlangState) start_else() {
	self.reg.cond_then = self.reg.expr
}

func (self *PlangState) construct_if() {
	expr := &ExprIf{
		If:   self.reg.cond_if,
		Then: self.reg.cond_then,
		Else: self.reg.expr,
	}
	self.pop()
	self.reg.expr = expr
}

func (self *PlangState) let_asg() {
	self.reg.let_var = self.reg.id
}

func (self *PlangState) let_in() {
	self.reg.let_val = self.reg.expr
}

func (self *PlangState) construct_let() {
	expr := &ExprLet{
		Var:  self.reg.let_var,
		Val:  self.reg.let_val,
		Expr: self.reg.expr,
	}
	self.pop()
	self.reg.expr = expr
}

func (self *PlangState) deffun_name() {
	self.fun_name = self.reg.id
	self.fun_args = []string{}
}

func (self *PlangState) deffun_arg() {
	self.fun_args = append(self.fun_args, self.reg.id)
}

func (self *PlangState) construct_deffun() {
	f := Fun{
		Name: self.fun_name,
		Args: self.fun_args,
		Body: self.reg.expr,
	}
	self.fun_args = []string{}
	self.functions = append(self.functions, f)
	self.order = append(self.order, f.Name)
}

func (self *PlangState) defvar_name() {
	self.var_name = self.reg.id
}

func (self *PlangState) set_printable() {
	self.var_printable = true
}

func (self *PlangState) set_printable_tp(txt string) {
	self.var_printable_tp = strings.TrimSpace(txt)
}

func (self *PlangState) construct_defvar() {
	printable := self.var_printable
	self.var_printable = false
	printable_tp := self.var_printable_tp
	self.var_printable_tp = ""

	f, parsed := output.StrToFmt(printable_tp)
	if !parsed {
		self.err = fmt.Errorf(
			"var '%s' has invalid out type: %s",
			self.var_name,
			printable_tp,
		)
	}
	v := Var{
		Name:    self.var_name,
		Body:    self.reg.expr,
		ToPrint: printable,
		Fmt:     f,
	}
	self.vars = append(self.vars, v)
	self.order = append(self.order, v.Name)
	self.var_name = ""
	self.reg.expr = nil
}

func (self *PlangState) start_dict() {
	self.push()
}

func (self *PlangState) add_dict_key() {
	self.construct_call_arg()
}

func (self *PlangState) add_dict_val() {
	self.construct_call_arg()
}

func (self *PlangState) construct_dict() {
	expr := &ExprCall{
		Fun:  ":dict",
		Args: self.reg.fun_args,
	}
	self.pop()
	self.reg.expr = expr
}

func (self *PlangState) start_uniq_set() {
	self.push()
	self.reg.fun_name = ":uset"
	self.reg.fun_args = []Expr{}
}

func (self *PlangState) start_not_uniq_set() {
	self.push()
	self.reg.fun_name = ":set"
	self.reg.fun_args = []Expr{}
}

func (self *PlangState) add_set_item() {
	self.construct_call_arg()
}

func (self *PlangState) construct_set() {
	self.end_call()
}
