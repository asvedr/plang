package grammar

import (
	"fmt"
	"strings"
)

type pretty_parse_error struct {
	err *parseError
}

func (self pretty_parse_error) Error() string {
	row, column := self.get_position()
	str_row := strings.Split(self.err.p.Buffer, "\n")[row-1]
	start := max(column-20, 0)
	substr := str_row[start:column]
	return fmt.Sprintf(
		"Parse error\n  row: \"%s\"\nafter seq:\n  \"%s\"",
		str_row,
		substr,
	)
}

func (self pretty_parse_error) get_position() (int, int) {
	tokens := []token32{self.err.max}
	positions, p := make([]int, 2*len(tokens)), 0
	for _, token := range tokens {
		positions[p], p = int(token.begin), p+1
		positions[p], p = int(token.end), p+1
	}
	translations := translatePositions(self.err.p.buffer, positions)
	begin := int(tokens[0].begin)
	row := translations[begin].line
	column := translations[begin].symbol
	return row, column
}
