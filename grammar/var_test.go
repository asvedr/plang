package grammar_test

import (
	"testing"

	"gitlab.com/asvedr/plang/entities/output"
	g "gitlab.com/asvedr/plang/grammar"
)

func TestGlobalVar(t *testing.T) {
	res, err := g.Parse("x is 1 + 2.")
	err_nil(t, err)
	v := res.Vars[0]
	body := g.MakeExprCall("+", &g.ExprInt{Val: 1}, &g.ExprInt{Val: 2})
	exp := g.Var{Name: "x", Body: body}
	equal(t, exp, v)
}

func TestOutVar(t *testing.T) {
	res, err := g.Parse("out x is 1.\ny is 2.")
	err_nil(t, err)
	equal(t, res.Vars[0].Name, "x")
	equal(t, res.Vars[0].ToPrint, true)
	equal(t, res.Vars[0].Fmt, output.Auto)
	equal(t, res.Vars[1].Name, "y")
	equal(t, res.Vars[1].ToPrint, false)
}

func TestOutVarCustomFmt(t *testing.T) {
	code := `
		out true x is 1.
		out variants y is 2.
		out avg z is 3.
	`
	res, err := g.Parse(code)
	err_nil(t, err)
	equal(t, res.Vars[0].Name, "x")
	equal(t, res.Vars[0].ToPrint, true)
	equal(t, res.Vars[0].Fmt, output.ProbTrue)
	equal(t, res.Vars[1].Name, "y")
	equal(t, res.Vars[1].ToPrint, true)
	equal(t, res.Vars[1].Fmt, output.ProbVariants)
	equal(t, res.Vars[2].Name, "z")
	equal(t, res.Vars[2].ToPrint, true)
	equal(t, res.Vars[2].Fmt, output.Avg)
}
