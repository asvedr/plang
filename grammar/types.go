package grammar

import (
	"fmt"
	"strings"

	"gitlab.com/asvedr/plang/entities/output"
)

type ExprType int

const (
	ETInt ExprType = iota
	ETBool
	ETReal
	ETSym
	ETVar
	ETCall
	ETOperator
	ETIf
	ETLet
)

var map_ExprType_to_string = map[ExprType]string{
	ETInt:      "int",
	ETReal:     "real",
	ETSym:      "sym",
	ETVar:      "var",
	ETCall:     "call",
	ETOperator: "operator",
	ETIf:       "if",
	ETLet:      "let",
	ETBool:     "bool",
}

func (self ExprType) String() string {
	val, found := map_ExprType_to_string[self]
	if !found {
		panic("unknown expr")
	}
	return val
}

type Fun struct {
	Name string
	Args []string
	Body Expr
}

type Var struct {
	Name    string
	Body    Expr
	ToPrint bool
	Fmt     output.Fmt
}

type Expr interface {
	Type() ExprType
	String() string
}

type Operator struct {
	Name string
}

type ExprVar struct {
	Name string
}

type ExprBool struct {
	Val bool
}

type ExprInt struct {
	Val int64
}

type ExprReal struct {
	Val float64
}

type ExprSym struct {
	Val string
}

type ExprCall struct {
	Fun  string
	Args []Expr
}

func MakeExprCall(f string, args ...Expr) Expr {
	if args == nil {
		args = []Expr{}
	}
	return &ExprCall{Fun: f, Args: args}
}

type ExprIf struct {
	If   Expr
	Then Expr
	Else Expr
}

type ExprLet struct {
	Var  string
	Val  Expr
	Expr Expr
}

func (*Operator) Type() ExprType {
	return ETOperator
}

func (self *Operator) String() string {
	return "op:" + self.Name
}

func (*ExprVar) Type() ExprType {
	return ETVar
}

func (self *ExprVar) String() string {
	return "var:" + self.Name
}

func (*ExprInt) Type() ExprType {
	return ETInt
}

func (self *ExprInt) String() string {
	return fmt.Sprintf("i%d", self.Val)
}

func (*ExprReal) Type() ExprType {
	return ETReal
}

func (self *ExprReal) String() string {
	return fmt.Sprintf("f%f", self.Val)
}

func (*ExprSym) Type() ExprType {
	return ETSym
}

func (self *ExprSym) String() string {
	return fmt.Sprintf("'%s", self.Val)
}

func (*ExprCall) Type() ExprType {
	return ETCall
}

func (self *ExprCall) String() string {
	args := []string{}
	for _, arg := range self.Args {
		args = append(args, arg.String())
	}
	return fmt.Sprintf("call:%s(%s)", self.Fun, strings.Join(args, ", "))
}

func (*ExprIf) Type() ExprType {
	return ETIf
}

func (self *ExprIf) String() string {
	return fmt.Sprintf("if %v ? %v : %v", self.If, self.Then, self.Else)
}

func (*ExprLet) Type() ExprType {
	return ETLet
}

func (self *ExprLet) String() string {
	return fmt.Sprintf("let %s = %v in %v", self.Var, self.Val, self.Expr)
}

func (*ExprBool) Type() ExprType {
	return ETBool
}

func (self *ExprBool) String() string {
	if self.Val {
		return "true"
	} else {
		return "false"
	}
}
