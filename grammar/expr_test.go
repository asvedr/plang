package grammar_test

import (
	"reflect"
	"testing"

	g "gitlab.com/asvedr/plang/grammar"
)

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func equal(t *testing.T, a, b any) {
	if !reflect.DeepEqual(a, b) {
		t.Fatalf("%v != %v", a, b)
	}
}

func parse_expr(src string) (g.Expr, error) {
	res, err := g.Parse("f is " + src + " .")
	if err != nil {
		return nil, err
	}
	return res.Vars[0].Body, nil
}

func TestAtom(t *testing.T) {
	e, err := parse_expr(" 12")
	err_nil(t, err)
	equal(t, &g.ExprInt{Val: 12}, e)

	e, err = parse_expr(" 'abc ")
	err_nil(t, err)
	equal(t, &g.ExprSym{Val: "abc"}, e)

	e, err = parse_expr(" true")
	err_nil(t, err)
	equal(t, &g.ExprBool{Val: true}, e)

	e, err = parse_expr(" false")
	err_nil(t, err)
	equal(t, &g.ExprBool{Val: false}, e)
}

func TestCall(t *testing.T) {
	e, err := parse_expr("f(1,2)")
	err_nil(t, err)
	equal(
		t,
		g.MakeExprCall("f", &g.ExprInt{Val: 1}, &g.ExprInt{Val: 2}),
		e,
	)
}

func TestSimpleOp(t *testing.T) {
	e, err := parse_expr("1 + 2")
	err_nil(t, err)
	expected := g.MakeExprCall(
		"+", &g.ExprInt{Val: 1}, &g.ExprInt{Val: 2},
	)
	equal(t, expected, e)
}

func TestComplexOp(t *testing.T) {
	e, err := parse_expr("1 + 2 = 4 - 1")
	err_nil(t, err)
	expected := g.MakeExprCall(
		"=",
		g.MakeExprCall(
			"+", &g.ExprInt{Val: 1}, &g.ExprInt{Val: 2},
		),
		g.MakeExprCall(
			"-", &g.ExprInt{Val: 4}, &g.ExprInt{Val: 1},
		),
	)
	equal(t, expected, e)
}

func TestTwiceUn(t *testing.T) {
	e, err := parse_expr("--2")
	err_nil(t, err)
	expected := g.MakeExprCall(
		"-",
		g.MakeExprCall("-", &g.ExprInt{Val: 2}),
	)
	equal(t, expected, e)
}

func TestOpWithUn(t *testing.T) {
	e, err := parse_expr("1 + --2 * f()")
	err_nil(t, err)
	mm2 := g.MakeExprCall(
		"-",
		g.MakeExprCall("-", &g.ExprInt{Val: 2}),
	)
	expected := g.MakeExprCall(
		"+",
		&g.ExprInt{Val: 1},
		g.MakeExprCall("*", mm2, g.MakeExprCall("f")),
	)
	equal(t, expected.String(), e.String())
}

func TestCond(t *testing.T) {
	e, err := parse_expr("if 1 + 2 = 3 ? 't : 'f")
	err_nil(t, err)
	expected := &g.ExprIf{
		If: g.MakeExprCall(
			"=",
			g.MakeExprCall("+", &g.ExprInt{Val: 1}, &g.ExprInt{Val: 2}),
			&g.ExprInt{Val: 3},
		),
		Then: &g.ExprSym{Val: "t"},
		Else: &g.ExprSym{Val: "f"},
	}
	equal(t, expected, e)
}

func TestLet(t *testing.T) {
	e, err := parse_expr("let A Is 1 + 2 in a + 3")
	err_nil(t, err)
	expected := &g.ExprLet{
		Var:  "a",
		Val:  g.MakeExprCall("+", &g.ExprInt{Val: 1}, &g.ExprInt{Val: 2}),
		Expr: g.MakeExprCall("+", &g.ExprVar{Name: "a"}, &g.ExprInt{Val: 3}),
	}
	equal(t, expected, e)
}
