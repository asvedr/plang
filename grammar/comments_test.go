package grammar_test

import (
	"testing"

	"gitlab.com/asvedr/plang/grammar"
)

const comments_src = `
x is 1 + 2. # assign x
y is wow. # beb
!x is x
`

const comments_res = `
x is 1 + 2.           
y is wow.      
!x is x
`

func TestRemoveComments(t *testing.T) {
	removed := grammar.RemoveComments(comments_src)
	if removed != comments_res {
		t.Fatal(removed)
	}
}
