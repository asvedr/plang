package grammar

import (
	"strings"
)

type Program struct {
	Funs  []Fun
	Vars  []Var
	Order []string
}

func (self *Program) GetFun(name string) *Fun {
	for _, fun := range self.Funs {
		if fun.Name == name {
			return &fun
		}
	}
	return nil
}

func (self *Program) GetVar(name string) *Var {
	for _, var_ := range self.Vars {
		if var_.Name == name {
			return &var_
		}
	}
	return nil
}

func Parse(src string) (Program, error) {
	src = RemoveComments(src)
	src = strings.TrimRight(src, " \t\n")
	plang := plang{Buffer: src}
	plang.state.Init()
	plang.Init()
	err := plang.Parse()
	if err != nil {
		perr, casted := err.(*parseError)
		if casted {
			err = pretty_parse_error{err: perr}
		}
		return Program{}, err
	}
	plang.Execute()
	if plang.state.err != nil {
		return Program{}, err
	}
	prog := Program{
		Funs:  plang.state.functions,
		Vars:  plang.state.vars,
		Order: plang.state.order,
	}
	return prog, err
}
