package grammar_test

import (
	"testing"

	g "gitlab.com/asvedr/plang/grammar"
)

func parse(src string) ([]g.Fun, error) {
	res, err := g.Parse(src)
	if err != nil {
		return nil, err
	}
	return res.Funs, nil
}

func Test2Funs(t *testing.T) {
	fns, err := parse("f1(x, y) is x + 1 .\nf2() is 1 .")
	err_nil(t, err)
	one := &g.ExprInt{Val: 1}
	f1 := g.Fun{
		Name: "f1",
		Args: []string{"x", "y"},
		Body: g.MakeExprCall("+", &g.ExprVar{Name: "x"}, one),
	}
	f2 := g.Fun{Name: "f2", Args: []string{}, Body: one}
	equal(t, []g.Fun{f1, f2}, fns)
}

func TestFuns(t *testing.T) {
	fns, err := parse("fact (x) is if x <= 1? 1: x * fact (x - 1).")
	err_nil(t, err)
	equal(t, 1, len(fns))
	x := &g.ExprVar{Name: "x"}
	one := &g.ExprInt{Val: 1}
	body := &g.ExprIf{
		If:   g.MakeExprCall("<=", x, one),
		Then: one,
		Else: g.MakeExprCall(
			"*",
			x,
			g.MakeExprCall("fact", g.MakeExprCall("-", x, one)),
		),
	}
	expected := g.Fun{Name: "fact", Args: []string{"x"}, Body: body}
	equal(t, expected, fns[0])
}
