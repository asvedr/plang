package stdlib

import (
	"errors"
	"reflect"

	errs "gitlab.com/asvedr/plang/entities/plangerr"
	v "gitlab.com/asvedr/plang/entities/value"
	p "gitlab.com/asvedr/plang/proto"
)

var int_ops = map[string]func(int64, int64) (int64, error){
	"*": func(a, b int64) (int64, error) { return a * b, nil },
	"/": func(a, b int64) (int64, error) {
		if b == 0 {
			return 0, errors.New("division by zero")
		}
		return a / b, nil
	},
}

var int_ord_ops = map[string]func(int64, int64) bool{
	">":  func(a, b int64) bool { return a > b },
	"<":  func(a, b int64) bool { return a < b },
	">=": func(a, b int64) bool { return a >= b },
	"<=": func(a, b int64) bool { return a <= b },
}

var real_ops = map[string]func(float64, float64) float64{
	"*": func(a, b float64) float64 { return a * b },
	"/": func(a, b float64) float64 { return a / b },
}

var real_ord_ops = map[string]func(float64, float64) bool{
	">":  func(a, b float64) bool { return a > b },
	"<":  func(a, b float64) bool { return a < b },
	">=": func(a, b float64) bool { return a >= b },
	"<=": func(a, b float64) bool { return a <= b },
}

var bool_ops = map[string]func(bool, bool) bool{
	"and": func(a, b bool) bool { return a && b },
	"or":  func(a, b bool) bool { return a || b },
}

func list_operators() []p.IFun {
	return []p.IFun{
		make_fn("*", num_op, "(num, num) -> num: numeric mul"),
		make_fn("/", num_op, "(num, num) -> num: numeric mul"),
		make_fn(">", num_ord_op, "(num, num) -> bool: check greater"),
		make_fn("<", num_ord_op, "(num, num) -> bool: check less"),
		make_fn(">=", num_ord_op, "(num, num) -> bool: check greater or equal"),
		make_fn("<=", num_ord_op, "(num, num) -> bool: check less or equal"),
		make_fn("=", eq_op, "(any, any) -> bool: check equal"),
		make_fn("<>", ne_op, "(any, any) -> bool: check not equal"),
		make_fn("and", bool_op, "(bool, bool) -> bool: boolean and"),
		make_fn("or", bool_op, "(bool, bool) -> bool: boolean or"),
		make_fn("!", not_op, "(bool) -> bool: boolean negate"),
		make_fn("%", mod_op, "(int, int) -> int: remainder of division"),
		make_fn("+", plus_op, "(num, num) -> num OR (set, set) -> set: numeric sum OR set concatenation"),
		make_fn("-", minus_op, "(num, num) -> num OR (num) -> num: numeric sub"),
	}
}

func mod_op(name string) StdFunF {
	counts := []int{2}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_num_args(name, args, counts)
		if err != nil {
			return nil, err
		}
		return v.Int(args[0].Int() % args[1].Int()), nil
	}
}

func plus_op(name string) StdFunF {
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		if len(args) != 2 {
			err := errs.NewFunCallInvalidArgNum(name, len(args), 2)
			return nil, err
		}
		a := args[0]
		b := args[1]
		at := a.Type()
		bt := b.Type()
		if at == v.VTSet && bt == v.VTSet {
			return v.Set(a.Set().Add(b.Set().Items()...)), nil
		}
		if !at.IsNum() || !bt.IsNum() {
			return nil, errs.NewFunCallInvalidArgType(
				name,
				[]v.ValueType{at, bt},
				[]errs.StrToStr{t_numeric, t_numeric},
			)
		}
		if at.WidestNum(bt) == v.VTReal {
			return v.Real(a.Real() + b.Real()), nil
		} else {
			return v.Int(a.Int() + b.Int()), nil
		}
	}
}

func minus_op(name string) StdFunF {
	zero := v.Int(0)
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		var a, b v.Value
		switch len(args) {
		case 2:
			a = args[0]
			b = args[1]
		case 1:
			a = zero
			b = args[0]
		default:
			return nil, errs.NewFunCallInvalidArgNum(name, len(args), 1, 2)
		}
		at := a.Type()
		bt := b.Type()
		if at == v.VTSet && bt == v.VTSet {
			return v.Set(a.Set().Del(b.Set().Items()...)), nil
		}
		if !at.IsNum() || !bt.IsNum() {
			return nil, errs.NewFunCallInvalidArgType(
				name,
				[]v.ValueType{at, bt},
				[]errs.StrToStr{t_numeric, t_numeric},
			)
		}
		if at.WidestNum(bt) == v.VTReal {
			return v.Real(a.Real() - b.Real()), nil
		} else {
			return v.Int(a.Int() - b.Int()), nil
		}
	}
}

func not_op(name string) StdFunF {
	types := []v.ValueType{v.VTBool}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_type_args(name, args, types)
		if err != nil {
			return nil, err
		}
		return v.Bool(!args[0].Bool()), nil
	}
}

func num_op(name string) StdFunF {
	int_op := int_ops[name]
	real_op := real_ops[name]
	counts := []int{2}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_num_args(name, args, counts)
		if err != nil {
			return nil, err
		}
		a := args[0]
		b := args[1]
		if a.Type().WidestNum(b.Type()) == v.VTReal {
			return v.Real(real_op(a.Real(), b.Real())), nil
		} else {
			res, err := int_op(a.Int(), b.Int())
			return v.Int(res), err
		}
	}
}

func num_ord_op(name string) StdFunF {
	int_op := int_ord_ops[name]
	real_op := real_ord_ops[name]
	counts := []int{2}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_num_args(name, args, counts)
		if err != nil {
			return nil, err
		}
		a := args[0]
		b := args[1]
		if a.Type().WidestNum(b.Type()) == v.VTReal {
			return v.Bool(real_op(a.Real(), b.Real())), nil
		} else {
			return v.Bool(int_op(a.Int(), b.Int())), nil
		}
	}
}

func bool_op(name string) StdFunF {
	op := bool_ops[name]
	types := []v.ValueType{v.VTBool, v.VTBool}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_type_args(name, args, types)
		if err != nil {
			return nil, err
		}
		a := args[0]
		b := args[1]
		return v.Bool(op(a.Bool(), b.Bool())), nil
	}
}

func eq_op(name string) StdFunF {
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_eq_args(name, args)
		if err != nil {
			return nil, err
		}
		return v.Bool(reflect.DeepEqual(args[0], args[1])), nil
	}
}

func ne_op(name string) StdFunF {
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_eq_args(name, args)
		if err != nil {
			return nil, err
		}
		return v.Bool(!reflect.DeepEqual(args[0], args[1])), nil
	}
}
