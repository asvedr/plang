package stdlib

import (
	"strconv"

	errs "gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/entities/value"
	v "gitlab.com/asvedr/plang/entities/value"
	p "gitlab.com/asvedr/plang/proto"
)

func list_convert() []p.IFun {
	return []p.IFun{
		make_fn("real", itor, "(num) -> real"),
		make_fn("sym", itos, "(num) -> sym"),
		make_fn("int", toi, "(num) -> int OR (bool) -> int"),
	}
}

func itor(name string) StdFunF {
	counts := []int{1}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_num_args(name, args, counts)
		if err != nil {
			return nil, err
		}
		return v.Real(args[0].Real()), nil
	}
}

func itos(name string) StdFunF {
	counts := []int{1}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_num_args(name, args, counts)
		if err != nil {
			return nil, err
		}
		sym := strconv.FormatInt(args[0].Int(), 10)
		return v.Symbol(sym), nil
	}
}

func toi(name string) StdFunF {
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		if len(args) != 1 {
			return nil, errs.NewFunCallInvalidArgNum(name, len(args), 1)
		}
		arg := args[0]
		switch arg.Type() {
		case value.VTBool:
			if arg.Bool() {
				return v.Int(1), nil
			} else {
				return v.Int(0), nil
			}
		case value.VTInt:
			return v.Int(arg.Int()), nil
		case value.VTReal:
			return v.Int(arg.Int()), nil
		default:
			return nil, errs.NewFunCallInvalidArgType(
				name,
				[]value.ValueType{arg.Type()},
				[]errs.StrToStr{"num or bool"},
			)
		}
	}
}
