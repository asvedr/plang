package stdlib

import (
	"gitlab.com/asvedr/plang/entities/prob_map"
	v "gitlab.com/asvedr/plang/entities/value"
	p "gitlab.com/asvedr/plang/proto"
)

func list_dict() []p.IFun {
	return []p.IFun{
		make_fn(":dict", make_dict, "<None>"),
	}
}

func make_dict(_ string) StdFunF {
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		dict := prob_map.New[v.Value]()
		for i := 0; i < len(args); i += 2 {
			dict.Add(args[i], args[i+1].Real())
		}
		return v.Dict(dict), nil
	}
}
