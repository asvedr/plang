package stdlib

import (
	errs "gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/entities/set"
	v "gitlab.com/asvedr/plang/entities/value"
	p "gitlab.com/asvedr/plang/proto"
)

func list_list() []p.IFun {
	return []p.IFun{
		make_fn(":uset", make_unque_set, "<None>"),
		make_fn(":set", make_not_uniq_set, "<None>"),
		make_fn("range", make_range, "(to: int) -> set OR (from: int, to: int) -> set: not uniq set of int"),
		make_fn("oneof", oneof, "(set) -> any: get random item of set"),
		make_fn("of", of, "(count: int, set) -> set: get subset of random peeked items"),
		make_fn("uniq", uniq, "(set) -> set: not uniq set to uniq set"),
		make_fn("not_uniq", not_uniq, "(set) -> set: uniq set to not uniq set"),
		make_fn("isin", isin, "(any, set) -> bool: check if item in set"),
	}
}

func make_unque_set(name string) StdFunF {
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		return v.Set(set.NewUniq(args)), nil
	}
}

func make_not_uniq_set(name string) StdFunF {
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		return v.Set(set.NewNotUniq(args)), nil
	}
}

func make_range(name string) StdFunF {
	zero := v.Int(0)
	exp1 := []v.ValueType{v.VTInt}
	exp2 := []v.ValueType{v.VTInt, v.VTInt}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		var from, to v.Value
		var t_args, exp []v.ValueType
		switch len(args) {
		case 1:
			from = zero
			to = args[0]
			t_args = append(t_args, to.Type())
			exp = exp1
		case 2:
			from = args[0]
			to = args[1]
			t_args = append(t_args, from.Type(), to.Type())
			exp = exp2
		default:
			return nil, errs.NewFunCallInvalidArgNum(
				name,
				len(args),
				1,
				2,
			)
		}
		if from.Type() != v.VTInt || to.Type() != v.VTInt {
			return nil, errs.NewFunCallInvalidArgType(name, t_args, exp)
		}
		result := []v.Value{}
		to_i := to.Int()
		for i := from.Int(); i < to_i; i += 1 {
			result = append(result, v.Int(i))
		}
		return v.Set(set.NewNotUniq(result)), nil
	}
}

func oneof(name string) StdFunF {
	expected := []v.ValueType{v.VTSet}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_type_args(name, args, expected)
		if err != nil {
			return nil, err
		}
		return args[0].Set().GetOneOf()
	}
}

func of(name string) StdFunF {
	expected := []v.ValueType{v.VTInt, v.VTSet}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_type_args(name, args, expected)
		if err != nil {
			return nil, err
		}
		count := args[0].Int()
		set := args[1].Set()
		subset, err := set.GetNOf(int(count))
		if err != nil {
			return nil, err
		}
		return v.Set(subset), nil
	}
}

func isin(name string) StdFunF {
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		if len(args) != 2 {
			return nil, errs.NewFunCallInvalidArgNum(name, len(args), 2)
		}
		if args[1].Type() != v.VTSet {
			return nil, errs.NewFunCallInvalidArgType(
				name,
				[]v.ValueType{args[0].Type(), args[1].Type()},
				[]errs.ToStr{t_any, v.VTSet},
			)
		}
		res := args[1].Set().Contains(args[0])
		return v.Bool(res), nil
	}
}

func uniq(name string) StdFunF {
	expected := []v.ValueType{v.VTSet}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_type_args(name, args, expected)
		if err != nil {
			return nil, err
		}
		return v.Set(args[0].Set().Unique()), nil
	}
}

func not_uniq(name string) StdFunF {
	expected := []v.ValueType{v.VTSet}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_type_args(name, args, expected)
		if err != nil {
			return nil, err
		}
		return v.Set(args[0].Set().NotUnique()), nil
	}
}
