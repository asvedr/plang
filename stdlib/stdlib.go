package stdlib

import (
	"sync"

	"gitlab.com/asvedr/plang/proto"
)

var funs_once_data []proto.IFun
var funs_once sync.Once

func global_funs_init() {
	result := list_operators()
	result = append(result, list_convert()...)
	result = append(result, list_list()...)
	result = append(result, list_dict()...)
	result = append(result, list_random()...)
	funs_once_data = result
}

func GlobalFuns() []proto.IFun {
	funs_once.Do(global_funs_init)
	return funs_once_data
}
