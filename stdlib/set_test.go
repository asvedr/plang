package stdlib_test

import (
	"reflect"
	"testing"

	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/impls/global_env"
	"gitlab.com/asvedr/plang/proto"
	"gitlab.com/asvedr/plang/stdlib"
)

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func equal(t *testing.T, a, b any) {
	if !reflect.DeepEqual(a, b) {
		t.Fatalf("%v != %v", a, b)
	}
}

func get_fun(funs []proto.IFun, name string) proto.IFun {
	for _, fun := range funs {
		if fun.Name() == name {
			return fun
		}
	}
	panic("fun '" + name + "' not found")
}

func TestListOneOf(t *testing.T) {
	funs := stdlib.GlobalFuns()
	f_list := get_fun(funs, ":set")
	f_oneof := get_fun(funs, "oneof")
	args := []value.Value{value.Symbol("a"), value.Symbol("b")}
	env := global_env.New().Copy(10)
	list, err := f_list.Call(env, args)
	err_nil(t, err)
	equal(t, "['a, 'b]", list.String())

	res, err := f_oneof.Call(env, []value.Value{list})
	err_nil(t, err)
	item := res.String()
	if item != `'a` && item != `'b` {
		t.Fatal(res.String())
	}
}
