package stdlib

import (
	"errors"
	"math/rand"

	v "gitlab.com/asvedr/plang/entities/value"
	p "gitlab.com/asvedr/plang/proto"
)

func list_random() []p.IFun {
	return []p.IFun{
		make_fn("randint", randint, "(a: int, b: int) -> int: random int in [a..b)"),
		make_fn("randreal", randreal, "(a: num, b: num) -> real: random real between a and b"),
		make_fn("randprob", randprob, "(dict) -> any: arg - {item: prob, ...}, res - peeked item"),
		make_fn("randnorm", randnorm, "(mean: real, stddev: real) -> real: peek num from normal distribution"),
		make_fn("randbool", randbool, "(prob: real) -> bool: get random bool(arg is prob of int)"),
	}
}

func randint(name string) StdFunF {
	expected := []v.ValueType{v.VTInt, v.VTInt}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_type_args(name, args, expected)
		if err != nil {
			return nil, err
		}
		from := args[0].Int()
		to := args[1].Int()
		shift := rand.Int63n(to - from)
		return v.Int(from + shift), nil
	}
}

func randbool(name string) StdFunF {
	expected := []v.ValueType{v.VTReal}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_type_args(name, args, expected)
		if err != nil {
			return nil, err
		}
		true_prob := args[0].Real()
		return v.Bool(rand.Float64() < true_prob), nil
	}
}

func randreal(name string) StdFunF {
	counts := []int{2}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_num_args(name, args, counts)
		if err != nil {
			return nil, err
		}
		from := args[0].Real()
		to := args[1].Real()
		res := from + (rand.Float64() * (to - from))
		return v.Real(res), nil
	}
}

func randprob(name string) StdFunF {
	expected := []v.ValueType{v.VTDict}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_type_args(name, args, expected)
		if err != nil {
			return nil, err
		}
		dict := args[0].Dict()
		if dict.Len() == 0 {
			return nil, errors.New("randprob: empty dict")
		}
		return dict.Take(), nil
	}
}

func randnorm(name string) StdFunF {
	counts := []int{2}
	return func(_ p.IGlobalEnv, args []v.Value) (v.Value, error) {
		err := validate_num_args(name, args, counts)
		if err != nil {
			return nil, err
		}
		mean := args[0].Real()
		std_dev := args[1].Real()
		res := rand.NormFloat64()*std_dev + mean
		return v.Real(res), nil
	}
}
