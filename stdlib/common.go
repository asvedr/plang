package stdlib

import (
	"fmt"

	errs "gitlab.com/asvedr/plang/entities/plangerr"
	v "gitlab.com/asvedr/plang/entities/value"
	p "gitlab.com/asvedr/plang/proto"
)

const t_numeric = errs.StrToStr("numeric")
const t_any = errs.StrToStr("any")

type StdFunF func(p.IGlobalEnv, []v.Value) (v.Value, error)
type StdFunFactory func(string) StdFunF

type StdFun struct {
	name string
	doc  string
	f    StdFunF
}

func (self StdFun) Name() string {
	return self.name
}

func (self StdFun) Call(e p.IGlobalEnv, a []v.Value) (v.Value, error) {
	err := e.IncStack()
	defer e.DecStack()
	if err != nil {
		return nil, err
	}
	return self.f(e, a)
}

func (self StdFun) Doc() string {
	return self.doc
}

func (self StdFun) String() string {
	return fmt.Sprintf("<native fun '%s'>", self.name)
}

func make_fn(
	name string,
	factory StdFunFactory,
	doc string,
) p.IFun {
	return StdFun{f: factory(name), name: name, doc: doc}
}

func validate_type_args(fn string, args []v.Value, types []v.ValueType) error {
	if len(args) != len(types) {
		return errs.NewFunCallInvalidArgNum(
			fn,
			len(args),
			len(types),
		)
	}
	found := false
	for i, arg := range args {
		if arg.Type() != types[i] {
			found = true
			break
		}
	}
	if !found {
		return nil
	}
	exp := []v.ValueType{}
	got := []v.ValueType{}
	for i, arg := range args {
		exp = append(exp, types[i])
		got = append(got, arg.Type())
	}
	return errs.NewFunCallInvalidArgType(fn, got, exp)
}

func validate_num_args(fn string, args []v.Value, counts []int) error {
	l := len(args)
	found := false
	for _, cnt := range counts {
		if cnt == l {
			found = true
			break
		}
	}
	if !found {
		return errs.NewFunCallInvalidArgNum(fn, len(args), counts...)
	}
	err := func() error {
		got := []v.ValueType{}
		exp := []errs.StrToStr{}
		for _, arg := range args {
			got = append(got, arg.Type())
			exp = append(exp, t_numeric)
		}
		return errs.NewFunCallInvalidArgType(fn, got, exp)
	}
	for _, arg := range args {
		if !arg.Type().IsNum() {
			return err()
		}
	}
	return nil
}

func validate_eq_args(fn string, args []v.Value) error {
	if len(args) != 2 {
		return errs.NewFunCallInvalidArgNum(fn, len(args), 2)
	}
	at := args[0].Type()
	bt := args[1].Type()
	if at != bt {
		return errs.NewFunCallInvalidArgType(
			fn,
			[]v.ValueType{at, bt},
			[]v.ValueType{at, at},
		)
	}
	if at == v.VTReal {
		return errs.FunCanNotAcceptType{Fun: fn, Tp: at}
	}
	return nil
}
