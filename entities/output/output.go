package output

type Fmt int

const (
	Auto Fmt = iota
	ProbTrue
	ProbVariants
	Avg
	Avgb
)

// STD=√[(∑(x-x_mid)^2) / n]

var fmt_str_map = map[Fmt]string{
	Auto:         "auto",
	ProbTrue:     "true",
	ProbVariants: "variants",
	Avg:          "avg",
	Avgb:         "avgb",
}

func (self Fmt) String() string {
	return fmt_str_map[self]
}

func StrToFmt(src string) (Fmt, bool) {
	if src == "" {
		return Auto, true
	}
	for res, key := range fmt_str_map {
		if key == src {
			return res, true
		}
	}
	return Auto, false
}
