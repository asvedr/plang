package vm_test

import (
	"reflect"
	"testing"

	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/entities/vm"
	"gitlab.com/asvedr/plang/impls/global_env"
	"gitlab.com/asvedr/plang/proto"
	"gitlab.com/asvedr/plang/stdlib"
)

func err_nil(t *testing.T, err error) {
	if err != nil {
		t.Fatal(err.Error())
	}
}

func equal(t *testing.T, a, b any) {
	if !reflect.DeepEqual(a, b) {
		t.Fatalf("%v != %v", a, b)
	}
}

func make_env() proto.IGlobalEnv {
	env := global_env.New().Copy(10)
	for _, fun := range stdlib.GlobalFuns() {
		env.RegFun(fun)
	}
	return env
}

func TestStdSub(t *testing.T) {
	env := make_env()
	sub := env.GetFun("-")
	res, err := sub.Call(env, []value.Value{value.Int(5), value.Int(2)})
	err_nil(t, err)
	equal(t, "3", res.String())
}

func TestAMinusB(t *testing.T) {
	env := make_env()
	sub := env.GetFun("-")
	code := []vm.Code{
		vm.CmdGetArg(0),
		vm.CmdGetArg(1),
		vm.CmdCallFun(sub, 2),
		vm.CmdRet(),
	}
	fun := vm.NewFun("a-b", code, 2, 0, 2)

	args := []value.Value{value.Int(5), value.Int(2)}
	val, err := fun.Call(env, args)
	err_nil(t, err)
	equal(t, int64(3), val.Int())

	args = []value.Value{value.Int(5), value.Int(-1)}
	val, err = fun.Call(env, args)
	err_nil(t, err)
	equal(t, int64(6), val.Int())
}

func fact_fun(env proto.IGlobalEnv) proto.IFun {
	eq := env.GetFun("<=")
	sub := env.GetFun("-")
	mul := env.GetFun("*")
	code := []vm.Code{
		vm.CmdGetArg(0),
		vm.CmdVal(value.Int(1)),
		vm.CmdCallFun(eq, 2),
		vm.CmdJumpOnFalse(3),
		vm.CmdVal(value.Int(1)),
		vm.CmdRet(),
		vm.CmdGetArg(0),
		vm.CmdGetArg(0),
		vm.CmdVal(value.Int(1)),
		vm.CmdCallFun(sub, 2),
		vm.CmdCallName("fact", 1),
		vm.CmdCallFun(mul, 2),
		vm.CmdRet(),
	}
	return vm.NewFun("fact", code, 4, 0, 1)
}

func TestFact(t *testing.T) {
	env := make_env().Copy(10)
	fun := fact_fun(env)

	env.RegFun(fun)

	res, err := fun.Call(env, []value.Value{value.Int(0)})
	err_nil(t, err)
	equal(t, int64(1), res.Int())

	res, err = fun.Call(env, []value.Value{value.Int(1)})
	err_nil(t, err)
	equal(t, int64(1), res.Int())

	res, err = fun.Call(env, []value.Value{value.Int(3)})
	err_nil(t, err)
	equal(t, int64(6), res.Int())

	res, err = fun.Call(env, []value.Value{value.Int(4)})
	err_nil(t, err)
	equal(t, int64(24), res.Int())
}

func TestInvalidArgs(t *testing.T) {
	env := make_env()
	fun := fact_fun(env)
	args := []value.Value{value.Symbol("abc")}
	_, err := fun.Call(env, args)
	exp := "fun '<=' is expecting (numeric, numeric) args, but found (symbol, int)"
	equal(t, exp, err.Error())
}

func TestInvalidArgCount(t *testing.T) {
	env := make_env()
	fun := fact_fun(env)
	args := []value.Value{value.Int(1), value.Int(2)}
	_, err := fun.Call(env, args)
	equal(t, "fun 'fact' is expecting 1 args but got 2", err.Error())
}

func TestFunNotRegistred(t *testing.T) {
	env := make_env()
	fun := fact_fun(env)
	args := []value.Value{value.Int(3)}
	_, err := fun.Call(env, args)
	equal(t, "FunNotFound(where: '<runtime>', fun: 'fact')", err.Error())
}

const fun_text = `fun 'fact' (stack=4, env=0, args=1):
  {get_arg, v:nil, s:"", i:0, f:nil}
  {val, v:1, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'<='}
  {jump_on_false, v:nil, s:"", i:3, f:nil}
  {val, v:1, s:"", i:0, f:nil}
  {ret, v:nil, s:"", i:0, f:nil}
  {get_arg, v:nil, s:"", i:0, f:nil}
  {get_arg, v:nil, s:"", i:0, f:nil}
  {val, v:1, s:"", i:0, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'-'}
  {call_name, v:nil, s:"fact", i:1, f:nil}
  {call_fun, v:nil, s:"", i:2, f:'*'}
  {ret, v:nil, s:"", i:0, f:nil}
`

func TestFunToString(t *testing.T) {
	fun := fact_fun(make_env())
	equal(t, fun_text, fun.String())
}
