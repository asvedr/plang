package vm

import (
	"fmt"

	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/proto"
)

type cmd int

const (
	c_val cmd = iota
	c_get_loc_var
	c_set_loc_var
	c_get_glob_var
	c_get_arg
	c_call_name
	c_call_fun
	c_jump
	c_jump_on_false
	c_ret
)

var cmd_to_str = map[cmd]string{
	c_val:           "val",
	c_get_loc_var:   "get_loc",
	c_set_loc_var:   "set_loc",
	c_get_glob_var:  "get_glob",
	c_get_arg:       "get_arg",
	c_call_name:     "call_name",
	c_call_fun:      "call_fun",
	c_jump:          "jump",
	c_jump_on_false: "jump_on_false",
	c_ret:           "ret",
}

type Code struct {
	cmd     cmd
	reg_val value.Value
	reg_str string
	reg_int int
	reg_fun proto.IFun
}

func CmdVal(val value.Value) Code {
	return Code{cmd: c_val, reg_val: val}
}

func CmdGetLocVar(id int) Code {
	return Code{cmd: c_get_loc_var, reg_int: id}
}

func CmdSetLocVar(id int) Code {
	return Code{cmd: c_set_loc_var, reg_int: id}
}

func CmdGetGlobVar(name string) Code {
	return Code{cmd: c_get_glob_var, reg_str: name}
}

func CmdGetArg(id int) Code {
	return Code{cmd: c_get_arg, reg_int: id}
}

func CmdCallName(name string, args int) Code {
	return Code{cmd: c_call_name, reg_str: name, reg_int: args}
}

func CmdCallFun(fun proto.IFun, args int) Code {
	return Code{cmd: c_call_fun, reg_fun: fun, reg_int: args}
}

func CmdJump(shift int) Code {
	return Code{cmd: c_jump, reg_int: shift}
}

func CmdJumpOnFalse(shift int) Code {
	return Code{cmd: c_jump_on_false, reg_int: shift}
}

func CmdRet() Code {
	return Code{cmd: c_ret}
}

const f_nil = "nil"

func (self Code) String() string {
	f := f_nil
	if self.reg_fun != nil {
		f = "'" + self.reg_fun.Name() + "'"
	}
	v := f_nil
	if self.reg_val != nil {
		v = self.reg_val.String()
	}
	return fmt.Sprintf(
		"{%s, v:%v, s:\"%v\", i:%v, f:%s}",
		cmd_to_str[self.cmd],
		v,
		self.reg_str,
		self.reg_int,
		f,
	)
}
