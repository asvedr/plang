package vm

import (
	"fmt"
	"strings"

	"gitlab.com/asvedr/gstack"
	errs "gitlab.com/asvedr/plang/entities/plangerr"
	"gitlab.com/asvedr/plang/entities/value"
	"gitlab.com/asvedr/plang/proto"
)

type fun struct {
	name       string
	code       []Code
	stack_size int
	env_size   int
	args       int
}

type fun_ctx struct {
	env   []value.Value
	stack gstack.Stack[value.Value]
}

func NewFun(
	name string,
	code []Code,
	stack_size int,
	env_size int,
	args_size int,
) proto.IFun {
	return &fun{
		name:       name,
		code:       code,
		env_size:   env_size,
		stack_size: stack_size,
		args:       args_size,
	}
}

func (self *fun) Name() string {
	return self.name
}

func (self *fun) Doc() string {
	return "<User defined>"
}

func (self *fun) Call(
	env proto.IGlobalEnv,
	args []value.Value,
) (value.Value, error) {
	err := env.IncStack()
	defer env.DecStack()
	if err != nil {
		return nil, err
	}
	if len(args) != self.args {
		return nil, errs.NewFunCallInvalidArgNum(self.name, len(args), self.args)
	}
	ctx := self.create_ctx(args)
	var shift int
	var val value.Value
loop:
	for i := 0; ; i += shift {
		shift = 1
		instr := self.code[i]
		switch instr.cmd {
		case c_val:
			ctx.stack.Push(instr.reg_val)
		case c_get_loc_var:
			ctx.stack.Push(ctx.env[instr.reg_int])
		case c_set_loc_var:
			ctx.env[instr.reg_int] = ctx.pop_stack()
		case c_get_glob_var:
			ctx.stack.Push(env.GetVal(instr.reg_str))
		case c_get_arg:
			ctx.stack.Push(args[instr.reg_int])
		case c_call_name:
			err := ctx.make_call_name(env, instr.reg_str, instr.reg_int)
			if err != nil {
				return nil, err
			}
		case c_call_fun:
			err := ctx.make_call_fun(env, instr.reg_fun, instr.reg_int)
			if err != nil {
				return nil, err
			}
		case c_jump:
			shift = instr.reg_int
		case c_jump_on_false:
			val = ctx.pop_stack()
			if val.Type() != value.VTBool {
				return nil, errs.CondMustBeBool{Tp: val.Type()}
			}
			if !val.Bool() {
				shift = instr.reg_int
			}
		case c_ret:
			break loop
		}
	}
	return ctx.pop_stack(), nil
}

func (self *fun) String() string {
	head := fmt.Sprintf(
		"fun '%s' (stack=%d, env=%d, args=%d):",
		self.name,
		self.stack_size,
		self.env_size,
		self.args,
	)
	str_code := []string{head}
	for _, instr := range self.code {
		str_code = append(str_code, "  "+instr.String())
	}
	return strings.Join(str_code, "\n") + "\n"
}

func (self *fun) create_ctx(args []value.Value) fun_ctx {
	return fun_ctx{
		env:   make([]value.Value, self.env_size),
		stack: gstack.NewVec[value.Value](self.stack_size),
	}
}

func (self fun_ctx) pop_stack() value.Value {
	val, got := self.stack.PopValue()
	if !got {
		panic("pop on empty stack!")
	}
	return val
}

func (self fun_ctx) make_call_name(
	env proto.IGlobalEnv,
	fname string,
	args_len int,
) error {
	fun := env.GetFun(fname)
	if fun == nil {
		return errs.FunNotFound{Fun: fname}
	}
	return self.make_call_fun(env, fun, args_len)
}

func (self fun_ctx) make_call_fun(
	env proto.IGlobalEnv,
	fun proto.IFun,
	args_len int,
) error {
	res, err := fun.Call(env, self.stack.PopRevSlice(args_len))
	if err != nil {
		return err
	}
	self.stack.Push(res)
	return nil
}
