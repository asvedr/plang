package set

import (
	"math/rand"
	"strings"

	"gitlab.com/asvedr/plang/entities/plangerr"
)

type Item interface {
	String() string
}

type Set[T Item] interface {
	Len() int
	String() string
	Add(...T) Set[T]
	Del(...T) Set[T]
	GetOneOf() (T, error)
	GetNOf(int) (Set[T], error)
	Contains(T) bool
	Items() []T
	Unique() Set[T]
	NotUnique() Set[T]
}

type set_uniq[T Item] struct {
	keys  map[string]int
	items []T
}

type set_not_uniq[T Item] struct {
	items []T
}

func NewUniq[T Item](items_orig []T) Set[T] {
	keys := map[string]int{}
	items := []T{}
	for _, item := range items_orig {
		key := item.String()
		_, found := keys[key]
		if found {
			continue
		}
		keys[item.String()] = len(items)
		items = append(items, item)
	}
	return &set_uniq[T]{items: items, keys: keys}
}

func NewNotUniq[T Item](items_orig []T) Set[T] {
	items := make([]T, len(items_orig))
	copy(items, items_orig)
	return &set_not_uniq[T]{items: items}
}

// --------------------------------------------------
// UNIQUE
// --------------------------------------------------

func (self *set_uniq[T]) Len() int {
	return len(self.items)
}

func (self *set_uniq[T]) String() string {
	keys := []string{}
	for _, item := range self.items {
		keys = append(keys, item.String())
	}
	return "u[" + strings.Join(keys, ", ") + "]"
}

func (self *set_uniq[T]) Add(items ...T) Set[T] {
	cp_keys := map[string]int{}
	cp_items := []T{}
	for i, item := range self.items {
		cp_items = append(cp_items, item)
		cp_keys[item.String()] = i
	}
	for _, item := range items {
		key := item.String()
		_, found := cp_keys[key]
		if !found {
			cp_keys[key] = len(cp_items)
			cp_items = append(cp_items, item)
		}
	}
	return &set_uniq[T]{items: cp_items, keys: cp_keys}
}

func (self *set_uniq[T]) Del(items ...T) Set[T] {
	to_exclude := map[string]bool{}
	for _, item := range items {
		to_exclude[item.String()] = true
	}
	cp_keys := map[string]int{}
	cp_items := []T{}
	for _, item := range self.items {
		key := item.String()
		if to_exclude[key] {
			continue
		}
		cp_keys[item.String()] = len(cp_items)
		cp_items = append(cp_items, item)
	}
	return &set_uniq[T]{items: cp_items, keys: cp_keys}
}

func (self *set_uniq[T]) GetOneOf() (T, error) {
	var item T
	if len(self.items) == 0 {
		return item, plangerr.OneOfEmptySet{}
	}
	i := rand.Intn(len(self.items))
	item = self.items[i]
	return item, nil
}

func (self *set_uniq[T]) GetNOf(n int) (Set[T], error) {
	if len(self.items) < n {
		err := plangerr.SetTooSmall{Expected: n, Got: len(self.items)}
		return nil, err
	}
	cp_items := make([]T, len(self.items))
	copy(cp_items, self.items)
	swap := func(i, j int) {
		cp_items[i], cp_items[j] = cp_items[j], cp_items[i]
	}
	rand.Shuffle(len(cp_items), swap)
	return NewUniq[T](cp_items[:n]), nil
}

func (self *set_uniq[T]) Contains(item T) bool {
	_, res := self.keys[item.String()]
	return res
}

func (self *set_uniq[T]) Items() []T {
	return self.items
}

func (self *set_uniq[T]) Unique() Set[T] {
	return self
}

func (self *set_uniq[T]) NotUnique() Set[T] {
	return NewNotUniq[T](self.items)
}

// --------------------------------------------------
// NOT UNIQUE
// --------------------------------------------------

func (self *set_not_uniq[T]) Len() int {
	return len(self.items)
}

func (self *set_not_uniq[T]) String() string {
	items := []string{}
	for _, item := range self.items {
		items = append(items, item.String())
	}
	return "[" + strings.Join(items, ", ") + "]"
}

func (self *set_not_uniq[T]) Add(suffix ...T) Set[T] {
	items := make([]T, len(self.items))
	copy(items, self.items)
	items = append(items, suffix...)
	return &set_not_uniq[T]{items: items}
}

func (self *set_not_uniq[T]) Del(to_del ...T) Set[T] {
	counter := map[string]int{}
	for _, item := range to_del {
		counter[item.String()] += 1
	}
	items := []T{}
	for _, item := range self.items {
		key := item.String()
		if counter[key] > 0 {
			counter[key] -= 1
		} else {
			items = append(items, item)
		}
	}
	return &set_not_uniq[T]{items: items}
}

func (self *set_not_uniq[T]) GetOneOf() (T, error) {
	var item T
	if len(self.items) == 0 {
		return item, plangerr.OneOfEmptySet{}
	}
	i := rand.Intn(len(self.items))
	return self.items[i], nil
}

func (self *set_not_uniq[T]) GetNOf(n int) (Set[T], error) {
	if len(self.items) < n {
		err := plangerr.SetTooSmall{Expected: n, Got: len(self.items)}
		return nil, err
	}
	cp_items := make([]T, len(self.items))
	copy(cp_items, self.items)
	swap := func(i, j int) {
		cp_items[i], cp_items[j] = cp_items[j], cp_items[i]
	}
	rand.Shuffle(len(cp_items), swap)
	return &set_not_uniq[T]{items: cp_items[:n]}, nil
}

func (self *set_not_uniq[T]) Contains(t T) bool {
	key := t.String()
	for _, item := range self.items {
		if item.String() == key {
			return true
		}
	}
	return false
}

func (self *set_not_uniq[T]) Items() []T {
	return self.items
}

func (self *set_not_uniq[T]) Unique() Set[T] {
	return NewUniq[T](self.items)
}

func (self *set_not_uniq[T]) NotUnique() Set[T] {
	return self
}
