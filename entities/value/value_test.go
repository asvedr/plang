package value_test

import (
	"math"
	"reflect"
	"testing"

	"gitlab.com/asvedr/plang/entities/prob_map"
	"gitlab.com/asvedr/plang/entities/set"
	"gitlab.com/asvedr/plang/entities/value"
)

func equal(t *testing.T, a, b any) {
	if !reflect.DeepEqual(a, b) {
		t.Fatalf("%v != %v", a, b)
	}
}

type setup struct {
	vint  value.Value
	vreal value.Value
	vsym  value.Value
	vbool value.Value
	vlist value.Value
	vdict value.Value
}

func new_setup() setup {
	set := set.NewUniq[value.Value](
		[]value.Value{value.Int(1), value.Symbol("a")},
	)
	dict := prob_map.New[value.Value]()
	dict.Add(value.Symbol("a"), 1.0)
	dict.Add(value.Symbol("b"), 2.0)
	return setup{
		vint:  value.Int(3),
		vreal: value.Real(2.1),
		vsym:  value.Symbol("abc"),
		vbool: value.Bool(true),
		vlist: value.Set(set),
		vdict: value.Dict(dict),
	}
}

func TestInt(t *testing.T) {
	s := new_setup()
	equal(t, s.vint.Type().IsNum(), true)
	equal(t, value.VTInt, s.vint.Type())
	equal(t, int64(3), s.vint.Int())
	equal(t, math.Abs(3.0-s.vint.Real()) < 0.01, true)
	equal(t, "3", s.vint.String())
}

func TestReal(t *testing.T) {
	s := new_setup()
	equal(t, s.vreal.Type().IsNum(), true)
	equal(t, value.VTReal, s.vreal.Type())
	equal(t, int64(2), s.vreal.Int())
	equal(t, math.Abs(2.1-s.vreal.Real()) < 0.01, true)
	equal(t, "2.1", s.vreal.String())
}

func TestSym(t *testing.T) {
	s := new_setup()
	equal(t, s.vsym.Type().IsNum(), false)
	equal(t, value.VTSymbol, s.vsym.Type())
	equal(t, "abc", s.vsym.Symbol())
	equal(t, "'abc", s.vsym.String())
}

func TestBool(t *testing.T) {
	s := new_setup()
	equal(t, s.vbool.Type().IsNum(), false)
	equal(t, value.VTBool, s.vbool.Type())
	equal(t, s.vbool.Bool(), true)
	equal(t, "true", s.vbool.String())
}

func TestWidestNum(t *testing.T) {
	s := new_setup()
	it := s.vint.Type()
	rt := s.vreal.Type()
	equal(t, it.WidestNum(it), value.VTInt)
	equal(t, rt.WidestNum(rt), value.VTReal)
	equal(t, rt.WidestNum(it), value.VTReal)
	equal(t, it.WidestNum(rt), value.VTReal)
}

func TestListItems(t *testing.T) {
	s := new_setup()
	set := s.vlist.Set()
	items := set.Items()
	equal(t, 2, set.Len())
	equal(t, int64(1), items[0].Int())
	equal(t, "a", items[1].Symbol())
}

func TestListToStr(t *testing.T) {
	s := new_setup()
	equal(t, "u[1, 'a]", s.vlist.String())
}

func TestDictToStr(t *testing.T) {
	s := new_setup()
	equal(t, "{'a: 1.000, 'b: 2.000}", s.vdict.String())
}
