package value

import (
	"fmt"

	"gitlab.com/asvedr/plang/entities/prob_map"
	"gitlab.com/asvedr/plang/entities/set"
)

type ValueType int

const (
	VTInt ValueType = iota
	VTReal
	VTSymbol
	VTBool
	VTSet
	VTDict
)

func (self ValueType) String() string {
	switch self {
	case VTInt:
		return "int"
	case VTReal:
		return "real"
	case VTSymbol:
		return "symbol"
	case VTBool:
		return "bool"
	case VTSet:
		return "set"
	case VTDict:
		return "dict"
	default:
		panic("unknown value type")
	}
}

func (self ValueType) IsNum() bool {
	return self == VTInt || self == VTReal
}

func (self ValueType) WidestNum(other ValueType) ValueType {
	if self == VTReal || other == VTReal {
		return VTReal
	} else {
		return VTInt
	}
}

type Value interface {
	Type() ValueType
	String() string
	Int() int64
	Real() float64
	Symbol() string
	Bool() bool
	Set() set.Set[Value]
	Dict() *prob_map.ProbMap[Value]
}

type val_int struct{ value int64 }
type val_real struct{ value float64 }
type val_symbol struct{ value string }
type val_bool struct{ value bool }
type val_set struct{ value set.Set[Value] }
type val_dict struct{ value *prob_map.ProbMap[Value] }

func Int(value int64) Value {
	return val_int{value: value}
}

func Real(value float64) Value {
	return val_real{value: value}
}

func Symbol(value string) Value {
	return val_symbol{value: value}
}

func Bool(value bool) Value {
	return val_bool{value: value}
}

func Set(value set.Set[Value]) Value {
	return val_set{value: value}
}

func Dict(value *prob_map.ProbMap[Value]) Value {
	return val_dict{value: value}
}

func (val_int) Type() ValueType    { return VTInt }
func (val_real) Type() ValueType   { return VTReal }
func (val_symbol) Type() ValueType { return VTSymbol }
func (val_bool) Type() ValueType   { return VTBool }
func (val_set) Type() ValueType    { return VTSet }
func (val_dict) Type() ValueType   { return VTDict }

func (self val_int) Int() int64                     { return self.value }
func (self val_int) Real() float64                  { return float64(self.value) }
func (self val_int) Symbol() string                 { panic("int is not sym") }
func (self val_int) Bool() bool                     { panic("int is not bool") }
func (self val_int) Set() set.Set[Value]            { panic("int is not set") }
func (self val_int) Dict() *prob_map.ProbMap[Value] { panic("int is not dict") }

func (self val_real) Int() int64                     { return int64(self.value) }
func (self val_real) Real() float64                  { return self.value }
func (self val_real) Symbol() string                 { panic("real is not sym") }
func (self val_real) Bool() bool                     { panic("real is not bool") }
func (self val_real) Set() set.Set[Value]            { panic("real is not set") }
func (self val_real) Dict() *prob_map.ProbMap[Value] { panic("real is not dict") }

func (self val_symbol) Int() int64                     { panic("sym is not int") }
func (self val_symbol) Real() float64                  { panic("sym is not real") }
func (self val_symbol) Symbol() string                 { return self.value }
func (self val_symbol) Bool() bool                     { panic("sym is not bool") }
func (self val_symbol) Set() set.Set[Value]            { panic("sym is not set") }
func (self val_symbol) Dict() *prob_map.ProbMap[Value] { panic("sym is not dict") }

func (self val_bool) Int() int64                     { panic("bool is not int") }
func (self val_bool) Real() float64                  { panic("bool is not real") }
func (self val_bool) Symbol() string                 { panic("bool is not sym") }
func (self val_bool) Bool() bool                     { return self.value }
func (self val_bool) Set() set.Set[Value]            { panic("bool is not set") }
func (self val_bool) Dict() *prob_map.ProbMap[Value] { panic("bool is not dict") }

func (self val_set) Int() int64                     { panic("set is not int") }
func (self val_set) Real() float64                  { panic("set is not real") }
func (self val_set) Symbol() string                 { panic("set is not sym") }
func (self val_set) Bool() bool                     { panic("set is not bool") }
func (self val_set) Set() set.Set[Value]            { return self.value }
func (self val_set) Dict() *prob_map.ProbMap[Value] { panic("set is not dict") }

func (self val_dict) Int() int64                     { panic("dict is not int") }
func (self val_dict) Real() float64                  { panic("dict is not real") }
func (self val_dict) Symbol() string                 { panic("dict is not sym") }
func (self val_dict) Bool() bool                     { panic("dict is not bool") }
func (self val_dict) Set() set.Set[Value]            { panic("dict is not set") }
func (self val_dict) Dict() *prob_map.ProbMap[Value] { return self.value }

func (self val_int) String() string {
	return fmt.Sprint(self.value)
}

func (self val_real) String() string {
	return fmt.Sprint(self.value)
}

func (self val_symbol) String() string {
	return "'" + self.value
}

func (self val_bool) String() string {
	if self.value {
		return "true"
	} else {
		return "false"
	}
}

func (self val_set) String() string {
	return self.value.String()
}

func (self val_dict) String() string {
	f := func(v Value) string { return v.String() }
	return self.value.String(f)
}
