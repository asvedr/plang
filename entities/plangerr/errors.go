package plangerr

import (
	"fmt"
	"strings"
)

type ToStr interface {
	String() string
}

type StrToStr string

func (self StrToStr) String() string {
	return string(self)
}

type GlobalNameAlreadyUsed struct {
	Name string
}

type VarNotFound struct {
	Where string
	Var   string
}

type FunNotFound struct {
	Where string
	Fun   string
}

type RuntimeError struct {
	Err  error
	Vars map[string]ToStr
}

type ValidationError struct {
	Err error
}

type OneOfEmptySet struct{}

type SetTooSmall struct {
	Expected int
	Got      int
}

type FunCallInvalidArgNum struct {
	Fun      string
	Expected []int
	Got      int
}

type FunCallInvalidArgType struct {
	Fun      string
	Expected []ToStr
	Got      []ToStr
}

type FunCanNotAcceptType struct {
	Fun string
	Tp  ToStr
}

type OutVarInvalidType struct {
	Var string
	Tp  ToStr
}

type CanNotChooseCollector struct {
	Var string
}

type CondMustBeBool struct{ Tp ToStr }

type StackOverflow struct{}

func (self CondMustBeBool) Error() string {
	return "condition must be bool but it's: " + self.Tp.String()
}

func (self StackOverflow) Error() string {
	return "stack overflow(possible infinity recursion)"
}

func (self SetTooSmall) Error() string {
	return fmt.Sprintf("set too small(required=%d, got=%d)", self.Expected, self.Got)
}

func (self OneOfEmptySet) Error() string {
	return "oneof on empty set"
}

func (self GlobalNameAlreadyUsed) Error() string {
	return "GlobalNameAlreadyUsed: " + self.Name
}

func (self VarNotFound) Error() string {
	return fmt.Sprintf("VarNotFound(where: '%s', var: '%s')", self.Where, self.Var)
}

func (self FunNotFound) Error() string {
	where := self.Where
	if where == "" {
		where = "<runtime>"
	}
	return fmt.Sprintf("FunNotFound(where: '%s', fun: '%s')", where, self.Fun)
}

func (self ValidationError) Error() string {
	return fmt.Sprintf("validation error: %v", self.Err)
}

func (self RuntimeError) Error() string {
	var vars []string
	for vname, val := range self.Vars {
		// vars = append(vars, vname)
		s_val := "<nil>"
		if val != nil {
			s_val = val.String()
		}
		vars = append(vars, fmt.Sprintf("  %s = %s", vname, s_val))
	}
	ctx := strings.Join(vars, "\n")
	return fmt.Sprintf("Runtime error: %v\n%s\n", self.Err, ctx)
}

func (self FunCallInvalidArgNum) Error() string {
	exp := []string{}
	for _, cnt := range self.Expected {
		exp = append(exp, fmt.Sprint(cnt))
	}
	return fmt.Sprintf(
		"fun '%s' is expecting %s args but got %d",
		self.Fun,
		strings.Join(exp, " or "),
		self.Got,
	)
}

func (self FunCallInvalidArgType) Error() string {
	exp := []string{}
	for _, arg := range self.Expected {
		exp = append(exp, arg.String())
	}
	got := []string{}
	for _, arg := range self.Got {
		got = append(got, arg.String())
	}
	return fmt.Sprintf(
		"fun '%s' is expecting (%s) args, but found (%s)",
		self.Fun,
		strings.Join(exp, ", "),
		strings.Join(got, ", "),
	)
}

func (self FunCanNotAcceptType) Error() string {
	return fmt.Sprintf(
		"fun '%s' can not accept %s",
		self.Fun,
		self.Tp.String(),
	)
}

func (self OutVarInvalidType) Error() string {
	return fmt.Sprintf(
		"output var '%s' has invalid type: %s",
		self.Var,
		self.Tp.String(),
	)
}

func (self CanNotChooseCollector) Error() string {
	return fmt.Sprintf("can not choose collector for var: '%s'", self.Var)
}

func NewFunCallInvalidArgNum(
	fun string,
	got int,
	expected ...int,
) error {
	return FunCallInvalidArgNum{Fun: fun, Got: got, Expected: expected}
}

func NewFunCallInvalidArgType[G, E ToStr](
	fun string,
	got []G,
	expected []E,
) error {
	i_got := []ToStr{}
	for _, arg := range got {
		i_got = append(i_got, arg)
	}
	i_exp := []ToStr{}
	for _, arg := range expected {
		i_exp = append(i_exp, arg)
	}
	return FunCallInvalidArgType{
		Fun:      fun,
		Got:      i_got,
		Expected: i_exp,
	}
}
