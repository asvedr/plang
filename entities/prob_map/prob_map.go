package prob_map

import (
	"fmt"
	"math/rand"
	"strings"
)

type ProbMap[T any] struct {
	items    []pair[T]
	prob_sum float64
}

type pair[T any] struct {
	obj  T
	prob float64
}

func New[T any]() *ProbMap[T] {
	return &ProbMap[T]{}
}

func (self *ProbMap[T]) Add(obj T, prob float64) {
	self.items = append(self.items, pair[T]{obj: obj, prob: prob})
	self.prob_sum += prob
}

func (self *ProbMap[T]) Len() int {
	return len(self.items)
}

func (self *ProbMap[T]) Take() T {
	rnd := rand.Float64() * self.prob_sum
	var last T
	for _, pair := range self.items {
		if rnd < pair.prob {
			return pair.obj
		}
		rnd -= pair.prob
		last = pair.obj
	}
	return last
}

func (self *ProbMap[T]) String(item_str func(T) string) string {
	rows := []string{}
	for _, pair := range self.items {
		row := fmt.Sprintf("%s: %.3f", item_str(pair.obj), pair.prob)
		rows = append(rows, row)
	}
	return "{" + strings.Join(rows, ", ") + "}"
}
